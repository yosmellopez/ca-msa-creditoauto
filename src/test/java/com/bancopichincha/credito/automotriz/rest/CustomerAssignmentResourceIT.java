package com.bancopichincha.credito.automotriz.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bancopichincha.credito.automotriz.IntegrationTest;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerAssignmentMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link com.bancopichincha.credito.automotriz.web.rest.CustomerAssignmentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
class CustomerAssignmentResourceIT {

    private static final LocalDate DEFAULT_ASSIGNMENT_DATE = LocalDate.ofEpochDay(0L);

    private static final LocalDate UPDATED_ASSIGNMENT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/customer-assignments";

    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();

    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CustomerAssignmentRepository customerAssignmentRepository;

    @Autowired
    private CustomerAssignmentMapper customerAssignmentMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerAssignmentMockMvc;

    private CustomerAssignment customerAssignment;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerAssignment createEntity(EntityManager em) {
        CustomerAssignment customerAssignment = new CustomerAssignment().assignmentDate(DEFAULT_ASSIGNMENT_DATE);
        return customerAssignment;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerAssignment createUpdatedEntity(EntityManager em) {
        CustomerAssignment customerAssignment = new CustomerAssignment().assignmentDate(UPDATED_ASSIGNMENT_DATE);
        return customerAssignment;
    }

    @BeforeEach
    public void initTest() {
        customerAssignment = createEntity(em);
    }

    @Test
    @Transactional
    void createCustomerAssignment() throws Exception {
        int databaseSizeBeforeCreate = customerAssignmentRepository.findAll().size();
        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);
        restCustomerAssignmentMockMvc
                .perform(
                        post(ENTITY_API_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isCreated());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerAssignment testCustomerAssignment = customerAssignmentList.get(customerAssignmentList.size() - 1);
        assertThat(testCustomerAssignment.getAssignmentDate()).isEqualTo(DEFAULT_ASSIGNMENT_DATE);
    }

    @Test
    @Transactional
    void createCustomerAssignmentWithExistingId() throws Exception {
        // Create the CustomerAssignment with an existing ID
        customerAssignment.setId(1L);
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        int databaseSizeBeforeCreate = customerAssignmentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerAssignmentMockMvc
                .perform(
                        post(ENTITY_API_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAssignmentDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerAssignmentRepository.findAll().size();
        // set the field null
        customerAssignment.setAssignmentDate(null);

        // Create the CustomerAssignment, which fails.
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        restCustomerAssignmentMockMvc
                .perform(
                        post(ENTITY_API_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCustomerAssignments() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        // Get all the customerAssignmentList
        restCustomerAssignmentMockMvc
                .perform(get(ENTITY_API_URL + "?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customerAssignment.getId().intValue())))
                .andExpect(jsonPath("$.[*].assignmentDate").value(hasItem(DEFAULT_ASSIGNMENT_DATE.toString())));
    }

    @Test
    @Transactional
    void getCustomerAssignment() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        // Get the customerAssignment
        restCustomerAssignmentMockMvc
                .perform(get(ENTITY_API_URL_ID, customerAssignment.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(customerAssignment.getId().intValue()))
                .andExpect(jsonPath("$.assignmentDate").value(DEFAULT_ASSIGNMENT_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingCustomerAssignment() throws Exception {
        // Get the customerAssignment
        restCustomerAssignmentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCustomerAssignment() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();

        // Update the customerAssignment
        CustomerAssignment updatedCustomerAssignment = customerAssignmentRepository.findById(customerAssignment.getId()).get();
        // Disconnect from session so that the updates on updatedCustomerAssignment are not directly saved in db
        em.detach(updatedCustomerAssignment);
        updatedCustomerAssignment.assignmentDate(UPDATED_ASSIGNMENT_DATE);
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(updatedCustomerAssignment);

        restCustomerAssignmentMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, customerAssignmentDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isOk());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerAssignment testCustomerAssignment = customerAssignmentList.get(customerAssignmentList.size() - 1);
        assertThat(testCustomerAssignment.getAssignmentDate()).isEqualTo(UPDATED_ASSIGNMENT_DATE);
    }

    @Test
    @Transactional
    void putNonExistingCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, customerAssignmentDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        put(ENTITY_API_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCustomerAssignmentWithPatch() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();

        // Update the customerAssignment using partial update
        CustomerAssignment partialUpdatedCustomerAssignment = new CustomerAssignment();
        partialUpdatedCustomerAssignment.setId(customerAssignment.getId());

        partialUpdatedCustomerAssignment.assignmentDate(UPDATED_ASSIGNMENT_DATE);

        restCustomerAssignmentMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCustomerAssignment.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerAssignment))
                )
                .andExpect(status().isOk());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerAssignment testCustomerAssignment = customerAssignmentList.get(customerAssignmentList.size() - 1);
        assertThat(testCustomerAssignment.getAssignmentDate()).isEqualTo(UPDATED_ASSIGNMENT_DATE);
    }

    @Test
    @Transactional
    void fullUpdateCustomerAssignmentWithPatch() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();

        // Update the customerAssignment using partial update
        CustomerAssignment partialUpdatedCustomerAssignment = new CustomerAssignment();
        partialUpdatedCustomerAssignment.setId(customerAssignment.getId());

        partialUpdatedCustomerAssignment.assignmentDate(UPDATED_ASSIGNMENT_DATE);

        restCustomerAssignmentMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCustomerAssignment.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCustomerAssignment))
                )
                .andExpect(status().isOk());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
        CustomerAssignment testCustomerAssignment = customerAssignmentList.get(customerAssignmentList.size() - 1);
        assertThat(testCustomerAssignment.getAssignmentDate()).isEqualTo(UPDATED_ASSIGNMENT_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, customerAssignmentDTO.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCustomerAssignment() throws Exception {
        int databaseSizeBeforeUpdate = customerAssignmentRepository.findAll().size();
        customerAssignment.setId(count.incrementAndGet());

        // Create the CustomerAssignment
        CustomerAssignmentDTO customerAssignmentDTO = customerAssignmentMapper.toDto(customerAssignment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCustomerAssignmentMockMvc
                .perform(
                        patch(ENTITY_API_URL)
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(customerAssignmentDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the CustomerAssignment in the database
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCustomerAssignment() throws Exception {
        // Initialize the database
        customerAssignmentRepository.saveAndFlush(customerAssignment);

        int databaseSizeBeforeDelete = customerAssignmentRepository.findAll().size();

        // Delete the customerAssignment
        restCustomerAssignmentMockMvc
                .perform(delete(ENTITY_API_URL_ID, customerAssignment.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustomerAssignment> customerAssignmentList = customerAssignmentRepository.findAll();
        assertThat(customerAssignmentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.bancopichincha.credito.automotriz.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bancopichincha.credito.automotriz.IntegrationTest;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.service.mapper.ExecutiveMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link com.bancopichincha.credito.automotriz.web.rest.ExecutiveResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
class ExecutiveResourceIT {

    private static final String DEFAULT_IDENTIFICATION = "AAAAAAAAAA";

    private static final String UPDATED_IDENTIFICATION = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";

    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";

    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";

    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";

    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CONVENTIONAL_TELEPHONE = "AAAAAAAAAA";

    private static final String UPDATED_CONVENTIONAL_TELEPHONE = "BBBBBBBBBB";

    private static final Integer DEFAULT_AGE = 1;

    private static final Integer UPDATED_AGE = 2;

    private static final String ENTITY_API_URL = "/api/executives";

    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();

    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExecutiveRepository executiveRepository;

    @Autowired
    private ExecutiveMapper executiveMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExecutiveMockMvc;

    private Executive executive;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Executive createEntity(EntityManager em) {
        Executive executive = new Executive()
                .identification(DEFAULT_IDENTIFICATION)
                .firstName(DEFAULT_FIRST_NAME)
                .lastName(DEFAULT_LAST_NAME)
                .address(DEFAULT_ADDRESS)
                .phoneNumber(DEFAULT_PHONE_NUMBER)
                .conventionalTelephone(DEFAULT_CONVENTIONAL_TELEPHONE)
                .age(DEFAULT_AGE);
        return executive;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Executive createUpdatedEntity(EntityManager em) {
        Executive executive = new Executive()
                .identification(UPDATED_IDENTIFICATION)
                .firstName(UPDATED_FIRST_NAME)
                .lastName(UPDATED_LAST_NAME)
                .address(UPDATED_ADDRESS)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .conventionalTelephone(UPDATED_CONVENTIONAL_TELEPHONE)
                .age(UPDATED_AGE);
        return executive;
    }

    @BeforeEach
    public void initTest() {
        executive = createEntity(em);
    }

    @Test
    @Transactional
    void createExecutive() throws Exception {
        int databaseSizeBeforeCreate = executiveRepository.findAll().size();
        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);
        restExecutiveMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isCreated());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeCreate + 1);
        Executive testExecutive = executiveList.get(executiveList.size() - 1);
        assertThat(testExecutive.getIdentification()).isEqualTo(DEFAULT_IDENTIFICATION);
        assertThat(testExecutive.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testExecutive.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testExecutive.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testExecutive.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testExecutive.getConventionalTelephone()).isEqualTo(DEFAULT_CONVENTIONAL_TELEPHONE);
        assertThat(testExecutive.getAge()).isEqualTo(DEFAULT_AGE);
    }

    @Test
    @Transactional
    void createExecutiveWithExistingId() throws Exception {
        // Create the Executive with an existing ID
        executive.setId(1L);
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        int databaseSizeBeforeCreate = executiveRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExecutiveMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isBadRequest());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkIdentificationIsRequired() throws Exception {
        int databaseSizeBeforeTest = executiveRepository.findAll().size();
        // set the field null
        executive.setIdentification(null);

        // Create the Executive, which fails.
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        restExecutiveMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isBadRequest());

        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = executiveRepository.findAll().size();
        // set the field null
        executive.setFirstName(null);

        // Create the Executive, which fails.
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        restExecutiveMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isBadRequest());

        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = executiveRepository.findAll().size();
        // set the field null
        executive.setLastName(null);

        // Create the Executive, which fails.
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        restExecutiveMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isBadRequest());

        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllExecutives() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        // Get all the executiveList
        restExecutiveMockMvc
                .perform(get(ENTITY_API_URL + "?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(executive.getId().intValue())))
                .andExpect(jsonPath("$.[*].identification").value(hasItem(DEFAULT_IDENTIFICATION)))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
                .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
                .andExpect(jsonPath("$.[*].conventionalTelephone").value(hasItem(DEFAULT_CONVENTIONAL_TELEPHONE)))
                .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE)));
    }

    @Test
    @Transactional
    void getExecutive() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        // Get the executive
        restExecutiveMockMvc
                .perform(get(ENTITY_API_URL_ID, executive.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(executive.getId().intValue()))
                .andExpect(jsonPath("$.identification").value(DEFAULT_IDENTIFICATION))
                .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
                .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
                .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
                .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
                .andExpect(jsonPath("$.conventionalTelephone").value(DEFAULT_CONVENTIONAL_TELEPHONE))
                .andExpect(jsonPath("$.age").value(DEFAULT_AGE));
    }

    @Test
    @Transactional
    void getNonExistingExecutive() throws Exception {
        // Get the executive
        restExecutiveMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExecutive() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();

        // Update the executive
        Executive updatedExecutive = executiveRepository.findById(executive.getId()).get();
        // Disconnect from session so that the updates on updatedExecutive are not directly saved in db
        em.detach(updatedExecutive);
        updatedExecutive
                .identification(UPDATED_IDENTIFICATION)
                .firstName(UPDATED_FIRST_NAME)
                .lastName(UPDATED_LAST_NAME)
                .address(UPDATED_ADDRESS)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .conventionalTelephone(UPDATED_CONVENTIONAL_TELEPHONE)
                .age(UPDATED_AGE);
        ExecutiveDTO executiveDTO = executiveMapper.toDto(updatedExecutive);

        restExecutiveMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, executiveDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isOk());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
        Executive testExecutive = executiveList.get(executiveList.size() - 1);
        assertThat(testExecutive.getIdentification()).isEqualTo(UPDATED_IDENTIFICATION);
        assertThat(testExecutive.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testExecutive.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testExecutive.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testExecutive.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testExecutive.getConventionalTelephone()).isEqualTo(UPDATED_CONVENTIONAL_TELEPHONE);
        assertThat(testExecutive.getAge()).isEqualTo(UPDATED_AGE);
    }

    @Test
    @Transactional
    void putNonExistingExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, executiveDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(executiveDTO)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExecutiveWithPatch() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();

        // Update the executive using partial update
        Executive partialUpdatedExecutive = new Executive();
        partialUpdatedExecutive.setId(executive.getId());

        partialUpdatedExecutive
                .firstName(UPDATED_FIRST_NAME)
                .address(UPDATED_ADDRESS)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .conventionalTelephone(UPDATED_CONVENTIONAL_TELEPHONE);

        restExecutiveMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedExecutive.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExecutive))
                )
                .andExpect(status().isOk());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
        Executive testExecutive = executiveList.get(executiveList.size() - 1);
        assertThat(testExecutive.getIdentification()).isEqualTo(DEFAULT_IDENTIFICATION);
        assertThat(testExecutive.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testExecutive.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testExecutive.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testExecutive.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testExecutive.getConventionalTelephone()).isEqualTo(UPDATED_CONVENTIONAL_TELEPHONE);
        assertThat(testExecutive.getAge()).isEqualTo(DEFAULT_AGE);
    }

    @Test
    @Transactional
    void fullUpdateExecutiveWithPatch() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();

        // Update the executive using partial update
        Executive partialUpdatedExecutive = new Executive();
        partialUpdatedExecutive.setId(executive.getId());

        partialUpdatedExecutive
                .identification(UPDATED_IDENTIFICATION)
                .firstName(UPDATED_FIRST_NAME)
                .lastName(UPDATED_LAST_NAME)
                .address(UPDATED_ADDRESS)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .conventionalTelephone(UPDATED_CONVENTIONAL_TELEPHONE)
                .age(UPDATED_AGE);

        restExecutiveMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedExecutive.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExecutive))
                )
                .andExpect(status().isOk());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
        Executive testExecutive = executiveList.get(executiveList.size() - 1);
        assertThat(testExecutive.getIdentification()).isEqualTo(UPDATED_IDENTIFICATION);
        assertThat(testExecutive.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testExecutive.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testExecutive.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testExecutive.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testExecutive.getConventionalTelephone()).isEqualTo(UPDATED_CONVENTIONAL_TELEPHONE);
        assertThat(testExecutive.getAge()).isEqualTo(UPDATED_AGE);
        ;
    }

    @Test
    @Transactional
    void patchNonExistingExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, executiveDTO.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExecutive() throws Exception {
        int databaseSizeBeforeUpdate = executiveRepository.findAll().size();
        executive.setId(count.incrementAndGet());

        // Create the Executive
        ExecutiveDTO executiveDTO = executiveMapper.toDto(executive);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExecutiveMockMvc
                .perform(
                        patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(executiveDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the Executive in the database
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExecutive() throws Exception {
        // Initialize the database
        executiveRepository.saveAndFlush(executive);

        int databaseSizeBeforeDelete = executiveRepository.findAll().size();

        // Delete the executive
        restExecutiveMockMvc
                .perform(delete(ENTITY_API_URL_ID, executive.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Executive> executiveList = executiveRepository.findAll();
        assertThat(executiveList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

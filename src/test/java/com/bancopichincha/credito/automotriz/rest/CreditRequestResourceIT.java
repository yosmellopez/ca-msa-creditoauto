package com.bancopichincha.credito.automotriz.rest;

import static com.bancopichincha.credito.automotriz.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bancopichincha.credito.automotriz.IntegrationTest;
import com.bancopichincha.credito.automotriz.domain.CreditRequest;
import com.bancopichincha.credito.automotriz.repository.CreditRequestRepository;
import com.bancopichincha.credito.automotriz.service.dto.CreditRequestDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CreditRequestMapper;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link com.bancopichincha.credito.automotriz.web.rest.CreditRequestResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
class CreditRequestResourceIT {

    private static final LocalDate DEFAULT_ELABORATION_DATE = LocalDate.now();

    private static final LocalDate UPDATED_ELABORATION_DATE = LocalDate.now().plusDays(1);

    private static final Integer DEFAULT_MONTHS_TERM = 1;

    private static final Integer UPDATED_MONTHS_TERM = 2;

    private static final Integer DEFAULT_DUES = 1;

    private static final Integer UPDATED_DUES = 2;

    private static final BigDecimal DEFAULT_INITIAL = new BigDecimal(1);

    private static final BigDecimal UPDATED_INITIAL = new BigDecimal(2);

    private static final String DEFAULT_OBSERVATION = "AAAAAAAAAA";

    private static final String UPDATED_OBSERVATION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/credit-requests";

    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();

    private static AtomicLong count = new AtomicLong(random.nextInt() + (2L * Integer.MAX_VALUE));

    @Autowired
    private CreditRequestRepository creditRequestRepository;

    @Autowired
    private CreditRequestMapper creditRequestMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCreditRequestMockMvc;

    private CreditRequest creditRequest;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CreditRequest createEntity(EntityManager em) {
        CreditRequest creditRequest = new CreditRequest()
                .elaborationDate(DEFAULT_ELABORATION_DATE)
                .monthsTerm(DEFAULT_MONTHS_TERM)
                .dues(DEFAULT_DUES)
                .initial(DEFAULT_INITIAL)
                .observation(DEFAULT_OBSERVATION);
        return creditRequest;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CreditRequest createUpdatedEntity(EntityManager em) {
        CreditRequest creditRequest = new CreditRequest()
                .elaborationDate(UPDATED_ELABORATION_DATE)
                .monthsTerm(UPDATED_MONTHS_TERM)
                .dues(UPDATED_DUES)
                .initial(UPDATED_INITIAL)
                .observation(UPDATED_OBSERVATION);
        return creditRequest;
    }

    @BeforeEach
    public void initTest() {
        creditRequest = createEntity(em);
    }

    @Test
    @Transactional
    void createCreditRequest() throws Exception {
        int databaseSizeBeforeCreate = creditRequestRepository.findAll().size();
        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);
        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isCreated());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeCreate + 1);
        CreditRequest testCreditRequest = creditRequestList.get(creditRequestList.size() - 1);
        assertThat(testCreditRequest.getElaborationDate()).isEqualTo(DEFAULT_ELABORATION_DATE);
        assertThat(testCreditRequest.getMonthsTerm()).isEqualTo(DEFAULT_MONTHS_TERM);
        assertThat(testCreditRequest.getDues()).isEqualTo(DEFAULT_DUES);
        assertThat(testCreditRequest.getInitial()).isEqualByComparingTo(DEFAULT_INITIAL);
        assertThat(testCreditRequest.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
    }

    @Test
    @Transactional
    void createCreditRequestWithExistingId() throws Exception {
        // Create the CreditRequest with an existing ID
        creditRequest.setId(1L);
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        int databaseSizeBeforeCreate = creditRequestRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkElaborationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRequestRepository.findAll().size();
        // set the field null
        creditRequest.setElaborationDate(null);

        // Create the CreditRequest, which fails.
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMonthsTermIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRequestRepository.findAll().size();
        // set the field null
        creditRequest.setMonthsTerm(null);

        // Create the CreditRequest, which fails.
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDuesIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRequestRepository.findAll().size();
        // set the field null
        creditRequest.setDues(null);

        // Create the CreditRequest, which fails.
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInitialIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRequestRepository.findAll().size();
        // set the field null
        creditRequest.setInitial(null);

        // Create the CreditRequest, which fails.
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        restCreditRequestMockMvc
                .perform(
                        post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCreditRequests() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        // Get all the creditRequestList
        restCreditRequestMockMvc
                .perform(get(ENTITY_API_URL + "?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(creditRequest.getId().intValue())))
                .andExpect(jsonPath("$.[*].elaborationDate").value(hasItem(DEFAULT_ELABORATION_DATE.toString())))
                .andExpect(jsonPath("$.[*].monthsTerm").value(hasItem(DEFAULT_MONTHS_TERM)))
                .andExpect(jsonPath("$.[*].dues").value(hasItem(DEFAULT_DUES)))
                .andExpect(jsonPath("$.[*].initial").value(hasItem(sameNumber(DEFAULT_INITIAL))))
                .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)));
    }

    @Test
    @Transactional
    void getCreditRequest() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        // Get the creditRequest
        restCreditRequestMockMvc
                .perform(get(ENTITY_API_URL_ID, creditRequest.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(creditRequest.getId().intValue()))
                .andExpect(jsonPath("$.elaborationDate").value(DEFAULT_ELABORATION_DATE.toString()))
                .andExpect(jsonPath("$.monthsTerm").value(DEFAULT_MONTHS_TERM))
                .andExpect(jsonPath("$.dues").value(DEFAULT_DUES))
                .andExpect(jsonPath("$.initial").value(sameNumber(DEFAULT_INITIAL)))
                .andExpect(jsonPath("$.observation").value(DEFAULT_OBSERVATION));
    }

    @Test
    @Transactional
    void getNonExistingCreditRequest() throws Exception {
        // Get the creditRequest
        restCreditRequestMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCreditRequest() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();

        // Update the creditRequest
        CreditRequest updatedCreditRequest = creditRequestRepository.findById(creditRequest.getId()).get();
        // Disconnect from session so that the updates on updatedCreditRequest are not directly saved in db
        em.detach(updatedCreditRequest);
        updatedCreditRequest
                .elaborationDate(UPDATED_ELABORATION_DATE)
                .monthsTerm(UPDATED_MONTHS_TERM)
                .dues(UPDATED_DUES)
                .initial(UPDATED_INITIAL)
                .observation(UPDATED_OBSERVATION);
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(updatedCreditRequest);

        restCreditRequestMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, creditRequestDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isOk());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
        CreditRequest testCreditRequest = creditRequestList.get(creditRequestList.size() - 1);
        assertThat(testCreditRequest.getElaborationDate()).isEqualTo(UPDATED_ELABORATION_DATE);
        assertThat(testCreditRequest.getMonthsTerm()).isEqualTo(UPDATED_MONTHS_TERM);
        assertThat(testCreditRequest.getDues()).isEqualTo(UPDATED_DUES);
        assertThat(testCreditRequest.getInitial()).isEqualByComparingTo(UPDATED_INITIAL);
        assertThat(testCreditRequest.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void putNonExistingCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, creditRequestDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCreditRequestWithPatch() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();

        // Update the creditRequest using partial update
        CreditRequest partialUpdatedCreditRequest = new CreditRequest();
        partialUpdatedCreditRequest.setId(creditRequest.getId());

        partialUpdatedCreditRequest.monthsTerm(UPDATED_MONTHS_TERM);

        restCreditRequestMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCreditRequest.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreditRequest))
                )
                .andExpect(status().isOk());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
        CreditRequest testCreditRequest = creditRequestList.get(creditRequestList.size() - 1);
        assertThat(testCreditRequest.getElaborationDate()).isEqualTo(DEFAULT_ELABORATION_DATE);
        assertThat(testCreditRequest.getMonthsTerm()).isEqualTo(UPDATED_MONTHS_TERM);
        assertThat(testCreditRequest.getDues()).isEqualTo(DEFAULT_DUES);
        assertThat(testCreditRequest.getInitial()).isEqualByComparingTo(DEFAULT_INITIAL);
        assertThat(testCreditRequest.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
    }

    @Test
    @Transactional
    void fullUpdateCreditRequestWithPatch() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();

        // Update the creditRequest using partial update
        CreditRequest partialUpdatedCreditRequest = new CreditRequest();
        partialUpdatedCreditRequest.setId(creditRequest.getId());

        partialUpdatedCreditRequest
                .elaborationDate(UPDATED_ELABORATION_DATE)
                .monthsTerm(UPDATED_MONTHS_TERM)
                .dues(UPDATED_DUES)
                .initial(UPDATED_INITIAL)
                .observation(UPDATED_OBSERVATION);

        restCreditRequestMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCreditRequest.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCreditRequest))
                )
                .andExpect(status().isOk());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
        CreditRequest testCreditRequest = creditRequestList.get(creditRequestList.size() - 1);
        assertThat(testCreditRequest.getElaborationDate()).isEqualTo(UPDATED_ELABORATION_DATE);
        assertThat(testCreditRequest.getMonthsTerm()).isEqualTo(UPDATED_MONTHS_TERM);
        assertThat(testCreditRequest.getDues()).isEqualTo(UPDATED_DUES);
        assertThat(testCreditRequest.getInitial()).isEqualByComparingTo(UPDATED_INITIAL);
        assertThat(testCreditRequest.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void patchNonExistingCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, creditRequestDTO.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCreditRequest() throws Exception {
        int databaseSizeBeforeUpdate = creditRequestRepository.findAll().size();
        creditRequest.setId(count.incrementAndGet());

        // Create the CreditRequest
        CreditRequestDTO creditRequestDTO = creditRequestMapper.toDto(creditRequest);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCreditRequestMockMvc
                .perform(
                        patch(ENTITY_API_URL)
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(creditRequestDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the CreditRequest in the database
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCreditRequest() throws Exception {
        // Initialize the database
        creditRequestRepository.saveAndFlush(creditRequest);

        int databaseSizeBeforeDelete = creditRequestRepository.findAll().size();

        // Delete the creditRequest
        restCreditRequestMockMvc
                .perform(delete(ENTITY_API_URL_ID, creditRequest.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CreditRequest> creditRequestList = creditRequestRepository.findAll();
        assertThat(creditRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

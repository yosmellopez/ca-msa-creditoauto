package com.bancopichincha.credito.automotriz.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bancopichincha.credito.automotriz.IntegrationTest;
import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CardYardMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link com.bancopichincha.credito.automotriz.web.rest.CardYardResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
class CardYardResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";

    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";

    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";

    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_POINT_SALE_NUMBER = 1;

    private static final Integer UPDATED_POINT_SALE_NUMBER = 2;

    private static final String ENTITY_API_URL = "/api/card-yards";

    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();

    private static AtomicLong count = new AtomicLong(random.nextInt() + (2L * Integer.MAX_VALUE));

    @Autowired
    private CardYardRepository cardYardRepository;

    @Autowired
    private CardYardMapper cardYardMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCardYardMockMvc;

    private CardYard cardYard;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardYard createEntity(EntityManager em) {
        CardYard cardYard = new CardYard()
                .name(DEFAULT_NAME)
                .phoneNumber(DEFAULT_PHONE_NUMBER)
                .address(DEFAULT_ADDRESS)
                .pointSaleNumber(DEFAULT_POINT_SALE_NUMBER);
        return cardYard;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardYard createUpdatedEntity(EntityManager em) {
        CardYard cardYard = new CardYard()
                .name(UPDATED_NAME)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .address(UPDATED_ADDRESS)
                .pointSaleNumber(UPDATED_POINT_SALE_NUMBER);
        return cardYard;
    }

    @BeforeEach
    public void initTest() {
        cardYard = createEntity(em);
    }

    @Test
    @Transactional
    void createCardYard() throws Exception {
        int databaseSizeBeforeCreate = cardYardRepository.findAll().size();
        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);
        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isCreated());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeCreate + 1);
        CardYard testCardYard = cardYardList.get(cardYardList.size() - 1);
        assertThat(testCardYard.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCardYard.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testCardYard.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testCardYard.getPointSaleNumber()).isEqualTo(DEFAULT_POINT_SALE_NUMBER);
    }

    @Test
    @Transactional
    void createCardYardWithExistingId() throws Exception {
        // Create the CardYard with an existing ID
        cardYard.setId(1L);
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        int databaseSizeBeforeCreate = cardYardRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isBadRequest());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardYardRepository.findAll().size();
        // set the field null
        cardYard.setName(null);

        // Create the CardYard, which fails.
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isBadRequest());

        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPhoneNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardYardRepository.findAll().size();
        // set the field null
        cardYard.setPhoneNumber(null);

        // Create the CardYard, which fails.
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isBadRequest());

        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardYardRepository.findAll().size();
        // set the field null
        cardYard.setAddress(null);

        // Create the CardYard, which fails.
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isBadRequest());

        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPointSaleNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = cardYardRepository.findAll().size();
        // set the field null
        cardYard.setPointSaleNumber(null);

        // Create the CardYard, which fails.
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        restCardYardMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isBadRequest());

        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCardYards() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        // Get all the cardYardList
        restCardYardMockMvc
                .perform(get(ENTITY_API_URL + "?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cardYard.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
                .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
                .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
                .andExpect(jsonPath("$.[*].pointSaleNumber").value(hasItem(DEFAULT_POINT_SALE_NUMBER)));
    }

    @Test
    @Transactional
    void getCardYard() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        // Get the cardYard
        restCardYardMockMvc
                .perform(get(ENTITY_API_URL_ID, cardYard.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(cardYard.getId().intValue()))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
                .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
                .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
                .andExpect(jsonPath("$.pointSaleNumber").value(DEFAULT_POINT_SALE_NUMBER));
    }

    @Test
    @Transactional
    void getNonExistingCardYard() throws Exception {
        // Get the cardYard
        restCardYardMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCardYard() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();

        // Update the cardYard
        CardYard updatedCardYard = cardYardRepository.findById(cardYard.getId()).get();
        // Disconnect from session so that the updates on updatedCardYard are not directly saved in db
        em.detach(updatedCardYard);
        updatedCardYard
                .name(UPDATED_NAME)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .address(UPDATED_ADDRESS)
                .pointSaleNumber(UPDATED_POINT_SALE_NUMBER);
        CardYardDTO cardYardDTO = cardYardMapper.toDto(updatedCardYard);

        restCardYardMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, cardYardDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isOk());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
        CardYard testCardYard = cardYardList.get(cardYardList.size() - 1);
        assertThat(testCardYard.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCardYard.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testCardYard.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCardYard.getPointSaleNumber()).isEqualTo(UPDATED_POINT_SALE_NUMBER);
    }

    @Test
    @Transactional
    void putNonExistingCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, cardYardDTO.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(
                        put(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cardYardDTO)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCardYardWithPatch() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();

        // Update the cardYard using partial update
        CardYard partialUpdatedCardYard = new CardYard();
        partialUpdatedCardYard.setId(cardYard.getId());

        partialUpdatedCardYard.name(UPDATED_NAME).address(UPDATED_ADDRESS);

        restCardYardMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCardYard.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCardYard))
                )
                .andExpect(status().isOk());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
        CardYard testCardYard = cardYardList.get(cardYardList.size() - 1);
        assertThat(testCardYard.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCardYard.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testCardYard.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCardYard.getPointSaleNumber()).isEqualTo(DEFAULT_POINT_SALE_NUMBER);
    }

    @Test
    @Transactional
    void fullUpdateCardYardWithPatch() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();

        // Update the cardYard using partial update
        CardYard partialUpdatedCardYard = new CardYard();
        partialUpdatedCardYard.setId(cardYard.getId());

        partialUpdatedCardYard
                .name(UPDATED_NAME)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .address(UPDATED_ADDRESS)
                .pointSaleNumber(UPDATED_POINT_SALE_NUMBER);

        restCardYardMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, partialUpdatedCardYard.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCardYard))
                )
                .andExpect(status().isOk());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
        CardYard testCardYard = cardYardList.get(cardYardList.size() - 1);
        assertThat(testCardYard.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCardYard.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testCardYard.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testCardYard.getPointSaleNumber()).isEqualTo(UPDATED_POINT_SALE_NUMBER);
    }

    @Test
    @Transactional
    void patchNonExistingCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, cardYardDTO.getId())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(
                        patch(ENTITY_API_URL_ID, count.incrementAndGet())
                                .contentType("application/merge-patch+json")
                                .content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isBadRequest());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCardYard() throws Exception {
        int databaseSizeBeforeUpdate = cardYardRepository.findAll().size();
        cardYard.setId(count.incrementAndGet());

        // Create the CardYard
        CardYardDTO cardYardDTO = cardYardMapper.toDto(cardYard);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardYardMockMvc
                .perform(
                        patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(cardYardDTO))
                )
                .andExpect(status().isMethodNotAllowed());

        // Validate the CardYard in the database
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCardYard() throws Exception {
        // Initialize the database
        cardYardRepository.saveAndFlush(cardYard);

        int databaseSizeBeforeDelete = cardYardRepository.findAll().size();

        // Delete the cardYard
        restCardYardMockMvc
                .perform(delete(ENTITY_API_URL_ID, cardYard.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CardYard> cardYardList = cardYardRepository.findAll();
        assertThat(cardYardList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

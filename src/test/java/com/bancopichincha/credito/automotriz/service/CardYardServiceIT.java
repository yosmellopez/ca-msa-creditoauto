package com.bancopichincha.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.service.impl.CardYardServiceImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig
public class CardYardServiceIT {

    @InjectMocks
    CardYardServiceImpl cardYardService;

    @Mock
    CardYardRepository cardYardRepository;

    @Test
    @DisplayName("Success: When CardYard no contains duplicate names")
    public void whenBrandContainsDuplicatesObjectsSuccess() {
        List<CardYard> cardYards = new ArrayList<>();
        cardYards.add(new CardYard().id(1L).name("Hyundai").address("Sector Central"));
        cardYards.add(new CardYard().id(2L).name("Toyota").address("Sector Central"));
        cardYards.add(new CardYard().id(3L).name("Kia").address("Sector Central"));
        try (MockedStatic<CSVHelper> mockedStatic = mockStatic(CSVHelper.class)) {
            mockedStatic.when(() -> CSVHelper.csvListBrand(any())).thenReturn(cardYards);
            when(cardYardRepository.saveAll(any())).thenReturn(cardYards);

            Resource brandResource = new ClassPathResource("config/liquibase/fake-data/card_yard.csv");
            cardYardService.initialLoad(brandResource.getFile());
            assertEquals(3, cardYards.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

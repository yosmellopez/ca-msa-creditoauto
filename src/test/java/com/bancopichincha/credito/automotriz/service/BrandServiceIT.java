package com.bancopichincha.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.service.impl.BrandServiceImpl;
import com.bancopichincha.credito.automotriz.web.rest.errors.DuplicateDataException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig
public class BrandServiceIT {

    @InjectMocks
    BrandServiceImpl brandService;

    @Mock
    BrandRepository brandRepository;

    @Test
    @DisplayName("Error: When brand contains duplicate names")
    public void whenBrandContainsDuplicatesObjectsError() {
        List<Brand> brands = new ArrayList<>();
        brands.add(new Brand().id(1L).brandName("Hyundai"));
        brands.add(new Brand().id(2L).brandName("Toyota"));
        brands.add(new Brand().id(3L).brandName("Toyota"));
        try (MockedStatic<CSVHelper> mockedStatic = mockStatic(CSVHelper.class)) {
            mockedStatic.when(() -> CSVHelper.csvListBrand(any())).thenReturn(brands);
            DuplicateDataException thrown = Assertions.assertThrows(DuplicateDataException.class, () -> {
                Resource brandResource = new ClassPathResource("config/liquibase/fake-data/brand.csv");
                brandService.initialLoad(brandResource.getFile());
            });
            assertEquals("Los datos de brands estan duplicados", thrown.getMessage());
        }
    }


    @Test
    @DisplayName("Success: When brand no contains duplicate names")
    public void whenBrandContainsDuplicatesObjectsSuccess() {
        List<Brand> brands = new ArrayList<>();
        brands.add(new Brand().id(1L).brandName("Hyundai"));
        brands.add(new Brand().id(2L).brandName("Toyota"));
        brands.add(new Brand().id(3L).brandName("Kia"));
        try (MockedStatic<CSVHelper> mockedStatic = mockStatic(CSVHelper.class)) {
            mockedStatic.when(() -> CSVHelper.csvListBrand(any())).thenReturn(brands);
            when(brandRepository.saveAll(any())).thenReturn(brands);

            Resource brandResource = new ClassPathResource("config/liquibase/fake-data/brand.csv");
            brandService.initialLoad(brandResource.getFile());
            assertEquals(3, brands.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

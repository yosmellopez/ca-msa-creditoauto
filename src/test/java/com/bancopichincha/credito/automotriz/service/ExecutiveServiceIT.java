package com.bancopichincha.credito.automotriz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.impl.ExecutiveServiceImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig
public class ExecutiveServiceIT {

    @InjectMocks
    ExecutiveServiceImpl executiveService;

    @Mock
    ExecutiveRepository executiveRepository;

    @Test
    @DisplayName("Success: When Executive no contains duplicate names")
    public void whenBrandContainsDuplicatesObjectsSuccess() {
        List<Executive> executives = new ArrayList<>();
        executives.add(new Executive().id(1L).firstName("Juan").lastName("Perez"));
        executives.add(new Executive().id(2L).firstName("Juan").lastName("Perez"));
        executives.add(new Executive().id(3L).firstName("Juan").lastName("Perez"));
        try (MockedStatic<CSVHelper> mockedStatic = mockStatic(CSVHelper.class)) {
            mockedStatic.when(() -> CSVHelper.csvListBrand(any())).thenReturn(executives);
            when(executiveRepository.saveAll(any())).thenReturn(executives);

            Resource brandResource = new ClassPathResource("config/liquibase/fake-data/executive.csv");
            executiveService.initialLoad(brandResource.getFile());
            assertEquals(3, executives.size());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.bancopichincha.credito.automotriz.config;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

public class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static SqlTestContainer devTestContainer;

    public void initialize(ConfigurableApplicationContext context) {
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        TestPropertyValues testValues = TestPropertyValues.empty();
        if (null == devTestContainer) {
            try {
                Class<? extends SqlTestContainer> containerClass = (Class<? extends SqlTestContainer>) Class.forName(
                        this.getClass().getPackageName() + ".PostgreSqlTestContainer"
                );
                devTestContainer = beanFactory.createBean(containerClass);
                beanFactory.registerSingleton(containerClass.getName(), devTestContainer);
                // ((DefaultListableBeanFactory)beanFactory).registerDisposableBean(containerClass.getName(), devTestContainer);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        testValues = testValues.and("spring.datasource.url=" + devTestContainer.getTestContainer().getJdbcUrl() + "");
        testValues = testValues.and("spring.datasource.username=" + devTestContainer.getTestContainer().getUsername());
        testValues = testValues.and("spring.datasource.password=" + devTestContainer.getTestContainer().getPassword());
        testValues = testValues.and("spring.datasource.type=com.zaxxer.hikari.HikariDataSource");
        testValues = testValues.and("spring.datasource.hikari.auto-commit=false");
        testValues = testValues.and("spring.datasource.hikari.poolName=Hikari");
        testValues.applyTo(context);
    }
}

package com.bancopichincha.credito.automotriz.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bancopichincha.credito.automotriz.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CreditRequestTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CreditRequest.class);
        CreditRequest creditRequest1 = new CreditRequest();
        creditRequest1.setId(1L);
        CreditRequest creditRequest2 = new CreditRequest();
        creditRequest2.setId(creditRequest1.getId());
        assertThat(creditRequest1).isEqualTo(creditRequest2);
        creditRequest2.setId(2L);
        assertThat(creditRequest1).isNotEqualTo(creditRequest2);
        creditRequest1.setId(null);
        assertThat(creditRequest1).isNotEqualTo(creditRequest2);
    }
}

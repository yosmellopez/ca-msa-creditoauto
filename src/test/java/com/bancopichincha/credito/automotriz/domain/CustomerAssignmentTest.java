package com.bancopichincha.credito.automotriz.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bancopichincha.credito.automotriz.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CustomerAssignmentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerAssignment.class);
        CustomerAssignment customerAssignment1 = new CustomerAssignment();
        customerAssignment1.setId(1L);
        CustomerAssignment customerAssignment2 = new CustomerAssignment();
        customerAssignment2.setId(customerAssignment1.getId());
        assertThat(customerAssignment1).isEqualTo(customerAssignment2);
        customerAssignment2.setId(2L);
        assertThat(customerAssignment1).isNotEqualTo(customerAssignment2);
        customerAssignment1.setId(null);
        assertThat(customerAssignment1).isNotEqualTo(customerAssignment2);
    }
}

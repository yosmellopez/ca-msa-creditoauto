package com.bancopichincha.credito.automotriz.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bancopichincha.credito.automotriz.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CardYardTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardYard.class);
        CardYard cardYard1 = new CardYard();
        cardYard1.setId(1L);
        CardYard cardYard2 = new CardYard();
        cardYard2.setId(cardYard1.getId());
        assertThat(cardYard1).isEqualTo(cardYard2);
        cardYard2.setId(2L);
        assertThat(cardYard1).isNotEqualTo(cardYard2);
        cardYard1.setId(null);
        assertThat(cardYard1).isNotEqualTo(cardYard2);
    }
}

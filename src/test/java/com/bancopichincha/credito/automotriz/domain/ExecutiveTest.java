package com.bancopichincha.credito.automotriz.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bancopichincha.credito.automotriz.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExecutiveTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Executive.class);
        Executive executive1 = new Executive();
        executive1.setId(1L);
        Executive executive2 = new Executive();
        executive2.setId(executive1.getId());
        assertThat(executive1).isEqualTo(executive2);
        executive2.setId(2L);
        assertThat(executive1).isNotEqualTo(executive2);
        executive1.setId(null);
        assertThat(executive1).isNotEqualTo(executive2);
    }
}

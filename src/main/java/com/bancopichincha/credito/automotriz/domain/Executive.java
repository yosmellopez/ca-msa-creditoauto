package com.bancopichincha.credito.automotriz.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Executive entity.\n@author The Pichincha team.
 */
@Entity
@Table(name = "executive")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Executive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "identification", nullable = false)
    private String identification;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "conventional_telephone")
    private String conventionalTelephone;

    @Column(name = "age")
    private Integer age;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "yard_where_works", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "fk_executive_card_yarn_id"))
    @JsonIgnoreProperties(value = {"vehicles"}, allowSetters = true)
    private CardYard yardWhereWorks;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Executive id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return this.identification;
    }

    public Executive identification(String identification) {
        this.setIdentification(identification);
        return this;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Executive firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Executive lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return this.address;
    }

    public Executive address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public Executive phoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getConventionalTelephone() {
        return this.conventionalTelephone;
    }

    public Executive conventionalTelephone(String conventionalTelephone) {
        this.setConventionalTelephone(conventionalTelephone);
        return this;
    }

    public void setConventionalTelephone(String conventionalTelephone) {
        this.conventionalTelephone = conventionalTelephone;
    }

    public Integer getAge() {
        return this.age;
    }

    public Executive age(Integer age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public CardYard getYardWhereWorks() {
        return this.yardWhereWorks;
    }

    public Executive yardWhereWorks(CardYard yardWhereWorks) {
        this.setYardWhereWorks(yardWhereWorks);
        return this;
    }

    public void setYardWhereWorks(CardYard yardWhereWorks) {
        this.yardWhereWorks = yardWhereWorks;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Executive)) {
            return false;
        }
        return id != null && id.equals(((Executive) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Executive{" +
                "id=" + getId() +
                ", identification='" + getIdentification() + "'" +
                ", firstName='" + getFirstName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", address='" + getAddress() + "'" +
                ", phoneNumber='" + getPhoneNumber() + "'" +
                ", conventionalTelephone='" + getConventionalTelephone() + "'" +
                ", age=" + getAge() +
                ", yardWhereWorks='" + getYardWhereWorks() + "'" +
                "}";
    }
}

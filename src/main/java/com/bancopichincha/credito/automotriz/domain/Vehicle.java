package com.bancopichincha.credito.automotriz.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Vehicle entity.\n@author The Pichincha team.
 */
@Entity
@Table(name = "vehicle")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "license_plate", nullable = false)
    private String licensePlate;

    @NotNull
    @Column(name = "model", nullable = false)
    private String model;

    @NotNull
    @Column(name = "chasis_number", nullable = false)
    private Integer chasisNumber;

    @Column(name = "type")
    private String type;

    @NotNull
    @Column(name = "cylinder_capacity", nullable = false)
    private Integer cylinderCapacity;

    @NotNull
    @Column(name = "appraisal", nullable = false)
    private String appraisal;

    @OneToOne
    @JoinColumn(unique = true)
    private Brand brand;

    @ManyToMany
    @JoinTable(
        name = "rel_vehicle_card_yard",
        joinColumns = @JoinColumn(name = "vehicle_id"),
        inverseJoinColumns = @JoinColumn(name = "card_yard_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "vehicles" }, allowSetters = true)
    private Set<CardYard> cardYards = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Vehicle id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return this.licensePlate;
    }

    public Vehicle licensePlate(String licensePlate) {
        this.setLicensePlate(licensePlate);
        return this;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getModel() {
        return this.model;
    }

    public Vehicle model(String model) {
        this.setModel(model);
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getChasisNumber() {
        return this.chasisNumber;
    }

    public Vehicle chasisNumber(Integer chasisNumber) {
        this.setChasisNumber(chasisNumber);
        return this;
    }

    public void setChasisNumber(Integer chasisNumber) {
        this.chasisNumber = chasisNumber;
    }

    public String getType() {
        return this.type;
    }

    public Vehicle type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCylinderCapacity() {
        return this.cylinderCapacity;
    }

    public Vehicle cylinderCapacity(Integer cylinderCapacity) {
        this.setCylinderCapacity(cylinderCapacity);
        return this;
    }

    public void setCylinderCapacity(Integer cylinderCapacity) {
        this.cylinderCapacity = cylinderCapacity;
    }

    public String getAppraisal() {
        return this.appraisal;
    }

    public Vehicle appraisal(String appraisal) {
        this.setAppraisal(appraisal);
        return this;
    }

    public void setAppraisal(String appraisal) {
        this.appraisal = appraisal;
    }

    public Brand getBrand() {
        return this.brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Vehicle brand(Brand brand) {
        this.setBrand(brand);
        return this;
    }

    public Set<CardYard> getCardYards() {
        return this.cardYards;
    }

    public void setCardYards(Set<CardYard> cardYards) {
        this.cardYards = cardYards;
    }

    public Vehicle cardYards(Set<CardYard> cardYards) {
        this.setCardYards(cardYards);
        return this;
    }

    public Vehicle addCardYard(CardYard cardYard) {
        this.cardYards.add(cardYard);
        cardYard.getVehicles().add(this);
        return this;
    }

    public Vehicle removeCardYard(CardYard cardYard) {
        this.cardYards.remove(cardYard);
        cardYard.getVehicles().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vehicle)) {
            return false;
        }
        return id != null && id.equals(((Vehicle) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Vehicle{" +
            "id=" + getId() +
            ", licensePlate='" + getLicensePlate() + "'" +
            ", model='" + getModel() + "'" +
            ", chasisNumber=" + getChasisNumber() +
            ", type='" + getType() + "'" +
            ", cylinderCapacity=" + getCylinderCapacity() +
            ", appraisal='" + getAppraisal() + "'" +
            "}";
    }
}

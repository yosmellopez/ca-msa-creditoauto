package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enumeration.CreditRequestStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Vehicle CreditRequest.\n@author The Pichincha team.
 */
@Entity
@Table(name = "credit_request")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CreditRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "elaboration_date", nullable = false)
    private LocalDate elaborationDate;

    @NotNull
    @Column(name = "months_term", nullable = false)
    private Integer monthsTerm;

    @NotNull
    @Column(name = "dues", nullable = false)
    private Integer dues;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @NotNull
    @Column(name = "initial", precision = 21, scale = 2, nullable = false)
    private BigDecimal initial;

    @Column(name = "observation")
    private String observation;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private CreditRequestStatus status;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = {"brand", "cardYards"}, allowSetters = true)
    private Vehicle vehicle;

    @NotNull
    @ManyToOne(optional = false)
    private Executive executive;

    @NotNull
    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = {"vehicles"}, allowSetters = true)
    private CardYard cardYard;

    @NotNull
    @ManyToOne(optional = false)
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CreditRequest id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getElaborationDate() {
        return this.elaborationDate;
    }

    public CreditRequest elaborationDate(LocalDate elaborationDate) {
        this.setElaborationDate(elaborationDate);
        return this;
    }

    public void setElaborationDate(LocalDate elaborationDate) {
        this.elaborationDate = elaborationDate;
    }

    public Integer getMonthsTerm() {
        return this.monthsTerm;
    }

    public CreditRequest monthsTerm(Integer monthsTerm) {
        this.setMonthsTerm(monthsTerm);
        return this;
    }

    public void setMonthsTerm(Integer monthsTerm) {
        this.monthsTerm = monthsTerm;
    }

    public Integer getDues() {
        return this.dues;
    }

    public CreditRequest dues(Integer dues) {
        this.setDues(dues);
        return this;
    }

    public void setDues(Integer dues) {
        this.dues = dues;
    }

    public BigDecimal getInitial() {
        return this.initial;
    }

    public CreditRequest initial(BigDecimal initial) {
        this.setInitial(initial);
        return this;
    }

    public void setInitial(BigDecimal initial) {
        this.initial = initial;
    }

    public String getObservation() {
        return this.observation;
    }

    public CreditRequest observation(String observation) {
        this.setObservation(observation);
        return this;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Vehicle getVehicle() {
        return this.vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public CreditRequest vehicle(Vehicle vehicle) {
        this.setVehicle(vehicle);
        return this;
    }

    public Executive getExecutive() {
        return this.executive;
    }

    public void setExecutive(Executive executive) {
        this.executive = executive;
    }

    public CreditRequest executive(Executive executive) {
        this.setExecutive(executive);
        return this;
    }

    public CardYard getCardYard() {
        return this.cardYard;
    }

    public void setCardYard(CardYard cardYard) {
        this.cardYard = cardYard;
    }

    public CreditRequest cardYard(CardYard cardYard) {
        this.setCardYard(cardYard);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CreditRequest customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public CreditRequest active(Boolean active) {
        this.setActive(active);
        return this;
    }

    public Boolean getActive() {
        return active;
    }

    public CreditRequestStatus getStatus() {
        return status;
    }

    public void setStatus(CreditRequestStatus status) {
        this.status = status;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CreditRequest)) {
            return false;
        }
        return id != null && id.equals(((CreditRequest) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CreditRequest{" +
                "id=" + getId() +
                ", elaborationDate='" + getElaborationDate() + "'" +
                ", monthsTerm=" + getMonthsTerm() +
                ", dues=" + getDues() +
                ", initial=" + getInitial() +
                ", observation='" + getObservation() + "'" +
                "}";
    }
}

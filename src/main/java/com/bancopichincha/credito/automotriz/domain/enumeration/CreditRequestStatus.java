package com.bancopichincha.credito.automotriz.domain.enumeration;

public enum CreditRequestStatus {
    REGISTERED, DISPATCHED, CANCELED;
}

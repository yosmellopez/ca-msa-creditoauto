package com.bancopichincha.credito.automotriz;

import com.bancopichincha.credito.automotriz.config.CRLFLogConverter;
import com.bancopichincha.credito.automotriz.service.BrandService;
import com.bancopichincha.credito.automotriz.service.CardYardService;
import com.bancopichincha.credito.automotriz.service.CustomerService;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import tech.jhipster.config.DefaultProfileUtil;

@SpringBootApplication
@EnableConfigurationProperties({LiquibaseProperties.class})
public class CaMsaCreditoautoApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CaMsaCreditoautoApplication.class);

    private final BrandService brandService;

    private final CustomerService customerService;

    private final ExecutiveService executiveService;

    private final CardYardService cardYardService;

    public CaMsaCreditoautoApplication(BrandService brandService, CustomerService customerService, ExecutiveService executiveService, CardYardService cardYardService) {
        this.brandService = brandService;
        this.customerService = customerService;
        this.executiveService = executiveService;
        this.cardYardService = cardYardService;
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CaMsaCreditoautoApplication.class);
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        logApplicationStartup(env);
    }

    private static void logApplicationStartup(Environment env) {
        String protocol = Optional.ofNullable(env.getProperty("server.ssl.key-store")).map(key -> "https").orElse("http");
        String serverPort = env.getProperty("server.port");
        String contextPath = Optional
                .ofNullable(env.getProperty("server.servlet.context-path"))
                .filter(StringUtils::isNotBlank)
                .orElse("/");
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info(
                CRLFLogConverter.CRLF_SAFE_MARKER,
                "\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\t{}://localhost:{}{}\n\t" +
                        "External: \t{}://{}:{}{}\n\t" +
                        "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                serverPort,
                contextPath,
                protocol,
                hostAddress,
                serverPort,
                contextPath,
                env.getActiveProfiles().length == 0 ? env.getDefaultProfiles() : env.getActiveProfiles()
        );
    }

    @Override
    public void run(String... args) throws Exception {
        Resource brandResource = new ClassPathResource("config/liquibase/fake-data/brand.csv");
        brandService.initialLoad(brandResource.getFile());
        Resource customerResource = new ClassPathResource("config/liquibase/fake-data/customer.csv");
        customerService.initialLoad(customerResource.getFile());
        Resource cardYardResource = new ClassPathResource("config/liquibase/fake-data/card_yard.csv");
        cardYardService.initialLoad(cardYardResource.getFile());
        Resource executiveResource = new ClassPathResource("config/liquibase/fake-data/executive.csv");
        executiveService.initialLoad(executiveResource.getFile());
    }
}

package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.CSVHelper;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.service.mapper.ExecutiveMapper;
import com.bancopichincha.credito.automotriz.web.rest.errors.DuplicateDataException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Executive}.
 */
@Service
@Transactional
public class ExecutiveServiceImpl implements ExecutiveService {

    private final Logger log = LoggerFactory.getLogger(ExecutiveServiceImpl.class);

    private final ExecutiveRepository executiveRepository;

    private final CardYardRepository cardYardRepository;

    private final ExecutiveMapper executiveMapper;

    public ExecutiveServiceImpl(ExecutiveRepository executiveRepository, CardYardRepository cardYardRepository, ExecutiveMapper executiveMapper) {
        this.executiveRepository = executiveRepository;
        this.cardYardRepository = cardYardRepository;
        this.executiveMapper = executiveMapper;
    }

    @Override
    public ExecutiveDTO save(ExecutiveDTO executiveDTO) {
        log.debug("Request to save Executive : {}", executiveDTO);
        Executive executive = executiveMapper.toEntity(executiveDTO);
        executive = executiveRepository.save(executive);
        return executiveMapper.toDto(executive);
    }

    @Override
    public ExecutiveDTO update(ExecutiveDTO executiveDTO) {
        log.debug("Request to update Executive : {}", executiveDTO);
        Executive executive = executiveMapper.toEntity(executiveDTO);
        executive = executiveRepository.save(executive);
        return executiveMapper.toDto(executive);
    }

    @Override
    public Optional<ExecutiveDTO> partialUpdate(ExecutiveDTO executiveDTO) {
        log.debug("Request to partially update Executive : {}", executiveDTO);

        return executiveRepository.findById(executiveDTO.getId()).map(existingExecutive -> {
            executiveMapper.partialUpdate(existingExecutive, executiveDTO);
            return existingExecutive;
        }).map(executiveRepository::save).map(executiveMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ExecutiveDTO> findAll() {
        log.debug("Request to get all Executives");
        return executiveRepository.findAll().stream().map(executiveMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ExecutiveDTO> findOne(Long id) {
        log.debug("Request to get Executive : {}", id);
        return executiveRepository.findById(id).map(executiveMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Executive : {}", id);
        executiveRepository.deleteById(id);
    }

    @Override
    public void initialLoad(File file) {
        try (InputStream inputStream = new FileInputStream(file)) {
            List<Executive> executiveList = CSVHelper.csvListExecutives(inputStream);
            List<Executive> list = executiveList.stream()
                    .filter(executive -> executiveRepository.findByIdentification(executive.getIdentification()).isEmpty())
                    .collect(Collectors.toList());
            Set<Executive> executives = new HashSet<>(list);
            if (list.size() != executives.size())
                throw new DuplicateDataException("Existen ejecutivos duplicados");
            for (Executive executive : executives) {
                CardYard yardWhereWorks = executive.getYardWhereWorks();
                Optional<CardYard> cardYardOptional = cardYardRepository.findByName(yardWhereWorks.getName());
                executive.setYardWhereWorks(cardYardOptional.orElseThrow(EntityNotFoundException::new));
            }
            executiveRepository.saveAll(executives);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

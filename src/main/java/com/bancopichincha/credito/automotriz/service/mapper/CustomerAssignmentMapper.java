package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link CustomerAssignment} and its DTO {@link CustomerAssignmentDTO}.
 */
@Mapper(componentModel = "spring")
public interface CustomerAssignmentMapper extends EntityMapper<CustomerAssignmentDTO, CustomerAssignment> {
//
//    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
//    @Mapping(target = "cardYard", source = "cardYard", qualifiedByName = "cardYardId")
//    CustomerAssignmentDTO toDto(CustomerAssignment s);
//
//    @Named("customerId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    CustomerDTO toDtoCustomerId(Customer customer);
//
//    @Named("cardYardId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    CardYardDTO toDtoCardYardId(CardYard cardYard);
}

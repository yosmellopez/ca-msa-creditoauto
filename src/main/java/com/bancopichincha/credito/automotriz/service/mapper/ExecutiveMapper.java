package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Executive} and its DTO {@link ExecutiveDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExecutiveMapper extends EntityMapper<ExecutiveDTO, Executive> {

}

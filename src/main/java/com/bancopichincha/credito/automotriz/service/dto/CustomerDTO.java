package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.pichincha.domain.Customer} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CustomerDTO implements Serializable {

    private Long id;

    /**
     * The firstname attribute.
     */
    @NotNull
    private String identification;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Integer age;

    @NotNull
    private String birthdDate;

    @NotNull
    private String address;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String maritalStatus;

    @NotNull
    private String spouseIdentification;

    @NotNull
    private String spouseFullName;

    private Boolean creditSubject;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBirthdDate() {
        return birthdDate;
    }

    public void setBirthdDate(String birthdDate) {
        this.birthdDate = birthdDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getSpouseIdentification() {
        return spouseIdentification;
    }

    public void setSpouseIdentification(String spouseIdentification) {
        this.spouseIdentification = spouseIdentification;
    }

    public String getSpouseFullName() {
        return spouseFullName;
    }

    public void setSpouseFullName(String spouseFullName) {
        this.spouseFullName = spouseFullName;
    }

    public Boolean getCreditSubject() {
        return creditSubject;
    }

    public void setCreditSubject(Boolean creditSubject) {
        this.creditSubject = creditSubject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerDTO)) {
            return false;
        }

        CustomerDTO customerDTO = (CustomerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, customerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerDTO{" +
                "id=" + getId() +
                ", identification='" + getIdentification() + "'" +
                ", firstName='" + getFirstName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", age=" + getAge() +
                ", birthdDate='" + getBirthdDate() + "'" +
                ", address='" + getAddress() + "'" +
                ", phoneNumber='" + getPhoneNumber() + "'" +
                ", maritalStatus='" + getMaritalStatus() + "'" +
                ", spouseIdentification='" + getSpouseIdentification() + "'" +
                ", spouseFullName='" + getSpouseFullName() + "'" +
                ", creditSubject='" + getCreditSubject() + "'" +
                "}";
    }
}

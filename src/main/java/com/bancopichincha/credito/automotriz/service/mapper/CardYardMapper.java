package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link CardYard} and its DTO {@link CardYardDTO}.
 */
@Mapper(componentModel = "spring")
public interface CardYardMapper extends EntityMapper<CardYardDTO, CardYard> {

}

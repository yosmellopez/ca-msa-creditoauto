package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.CreditRequest;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.Executive;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.dto.CreditRequestDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDTO;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link CreditRequest} and its DTO {@link CreditRequestDTO}.
 */
@Mapper(componentModel = "spring")
public interface CreditRequestMapper extends EntityMapper<CreditRequestDTO, CreditRequest> {

//    @Mapping(target = "vehicle", source = "vehicle", qualifiedByName = "vehicleId")
//    @Mapping(target = "executive", source = "executive", qualifiedByName = "executiveId")
//    @Mapping(target = "cardYard", source = "cardYard", qualifiedByName = "cardYardId")
//    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
//    CreditRequestDTO toDto(CreditRequest s);
//
//    @Named("vehicleId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    VehicleDTO toDtoVehicleId(Vehicle vehicle);
//
//    @Named("executiveId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    ExecutiveDTO toDtoExecutiveId(Executive executive);
//
//    @Named("cardYardId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    CardYardDTO toDtoCardYardId(CardYard cardYard);
//
//    @Named("customerId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    CustomerDTO toDtoCustomerId(Customer customer);
}

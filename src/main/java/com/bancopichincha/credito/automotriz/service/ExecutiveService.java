package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.pichincha.domain.Executive}.
 */
public interface ExecutiveService {

    /**
     * Save a executive.
     *
     * @param executiveDTO the entity to save.
     * @return the persisted entity.
     */
    ExecutiveDTO save(ExecutiveDTO executiveDTO);

    /**
     * Updates a executive.
     *
     * @param executiveDTO the entity to update.
     * @return the persisted entity.
     */
    ExecutiveDTO update(ExecutiveDTO executiveDTO);

    /**
     * Partially updates a executive.
     *
     * @param executiveDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ExecutiveDTO> partialUpdate(ExecutiveDTO executiveDTO);

    /**
     * Get all the executives.
     *
     * @return the list of entities.
     */
    List<ExecutiveDTO> findAll();

    /**
     * Get the "id" executive.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExecutiveDTO> findOne(Long id);

    /**
     * Delete the "id" executive.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void initialLoad(File file);
}

package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CreditRequestDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.bancopichincha.credito.automotriz.domain.CreditRequest}.
 */
public interface CreditRequestService {

    /**
     * Save a creditRequest.
     *
     * @param creditRequestDTO the entity to save.
     * @return the persisted entity.
     */
    CreditRequestDTO save(CreditRequestDTO creditRequestDTO);

    /**
     * Updates a creditRequest.
     *
     * @param creditRequestDTO the entity to update.
     * @return the persisted entity.
     */
    CreditRequestDTO update(CreditRequestDTO creditRequestDTO);

    /**
     * Partially updates a creditRequest.
     *
     * @param creditRequestDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CreditRequestDTO> partialUpdate(CreditRequestDTO creditRequestDTO);

    /**
     * Get all the creditRequests.
     *
     * @return the list of entities.
     */
    List<CreditRequestDTO> findAll();

    /**
     * Get the "id" creditRequest.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CreditRequestDTO> findOne(Long id);

    /**
     * Delete the "id" creditRequest.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void cancel(Long id);

    void dispatch(Long id);
}

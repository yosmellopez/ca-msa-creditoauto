package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerAssignmentMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CustomerAssignment}.
 */
@Service
@Transactional
public class CustomerAssignmentServiceImpl implements CustomerAssignmentService {

    private final Logger log = LoggerFactory.getLogger(CustomerAssignmentServiceImpl.class);

    private final CustomerAssignmentRepository customerAssignmentRepository;

    private final CustomerAssignmentMapper customerAssignmentMapper;

    public CustomerAssignmentServiceImpl(CustomerAssignmentRepository customerAssignmentRepository, CustomerAssignmentMapper customerAssignmentMapper) {
        this.customerAssignmentRepository = customerAssignmentRepository;
        this.customerAssignmentMapper = customerAssignmentMapper;
    }

    @Override
    public CustomerAssignmentDTO save(CustomerAssignmentDTO customerAssignmentDTO) {
        log.debug("Request to save CustomerAssignment : {}", customerAssignmentDTO);
        CustomerAssignment customerAssignment = customerAssignmentMapper.toEntity(customerAssignmentDTO);
        customerAssignment = customerAssignmentRepository.save(customerAssignment);
        return customerAssignmentMapper.toDto(customerAssignment);
    }

    @Override
    public CustomerAssignmentDTO update(CustomerAssignmentDTO customerAssignmentDTO) {
        log.debug("Request to update CustomerAssignment : {}", customerAssignmentDTO);
        CustomerAssignment customerAssignment = customerAssignmentMapper.toEntity(customerAssignmentDTO);
        customerAssignment = customerAssignmentRepository.save(customerAssignment);
        return customerAssignmentMapper.toDto(customerAssignment);
    }

    @Override
    public Optional<CustomerAssignmentDTO> partialUpdate(CustomerAssignmentDTO customerAssignmentDTO) {
        log.debug("Request to partially update CustomerAssignment : {}", customerAssignmentDTO);

        return customerAssignmentRepository.findById(customerAssignmentDTO.getId()).map(existingCustomerAssignment -> {
            customerAssignmentMapper.partialUpdate(existingCustomerAssignment, customerAssignmentDTO);

            return existingCustomerAssignment;
        }).map(customerAssignmentRepository::save).map(customerAssignmentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerAssignmentDTO> findAll() {
        log.debug("Request to get all CustomerAssignments");
        return customerAssignmentRepository.findAll().stream().map(customerAssignmentMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerAssignmentDTO> findOne(Long id) {
        log.debug("Request to get CustomerAssignment : {}", id);
        return customerAssignmentRepository.findById(id).map(customerAssignmentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CustomerAssignment : {}", id);
        customerAssignmentRepository.deleteById(id);
    }
}

package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.Executive;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CSVHelper {

    public static List<Brand> csvListBrand(InputStream is) {
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setSkipHeaderRecord(true)
                .setIgnoreHeaderCase(true)
                .setTrim(true)
                .setDelimiter(';')
                .setHeader("id", "brand_name")
                .build();
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)); CSVParser csvParser = new CSVParser(fileReader, csvFormat);) {
            List<Brand> brands = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                Brand brand = new Brand()
                        .id(Long.parseLong(csvRecord.get("id")))
                        .brandName(csvRecord.get("brand_name"));
                brands.add(brand);
            }
            return brands;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

    public static List<Customer> csvListCustomers(InputStream is) {
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setSkipHeaderRecord(true)
                .setIgnoreHeaderCase(true)
                .setTrim(true)
                .setDelimiter(';')
                .setHeader("id", "identification", "first_name", "last_name", "age", "birthd_date", "address",
                        "phone_number", "marital_status", "spouse_identification", "spouse_full_name", "credit_subject")
                .build();
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)); CSVParser csvParser = new CSVParser(fileReader, csvFormat);) {
            List<Customer> customers = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                Customer brand = new Customer()
                        .id(Long.parseLong(csvRecord.get("id")))
                        .identification(csvRecord.get("identification"))
                        .firstName(csvRecord.get("first_name"))
                        .lastName(csvRecord.get("last_name"))
                        .age(Integer.parseInt(csvRecord.get("age")))
                        .birthdDate(LocalDate.parse(csvRecord.get("birthd_date")))
                        .address(csvRecord.get("address"))
                        .phoneNumber(csvRecord.get("phone_number"))
                        .maritalStatus(csvRecord.get("marital_status"))
                        .spouseIdentification(csvRecord.get("spouse_identification"))
                        .spouseFullName(csvRecord.get("spouse_full_name"))
                        .creditSubject(Boolean.parseBoolean(csvRecord.get("credit_subject")));
                customers.add(brand);
            }
            return customers;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

    public static List<CardYard> csvListCardYarns(InputStream is) {
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setSkipHeaderRecord(true)
                .setIgnoreHeaderCase(true)
                .setTrim(true)
                .setDelimiter(';')
                .setHeader("id", "name", "phone_number", "address", "point_sale_number")
                .build();
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)); CSVParser csvParser = new CSVParser(fileReader, csvFormat);) {
            List<CardYard> cardYards = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                CardYard cardYard = new CardYard()
                        .name(csvRecord.get("name"))
                        .phoneNumber(csvRecord.get("phone_number"))
                        .address(csvRecord.get("address"))
                        .pointSaleNumber(Integer.parseInt(csvRecord.get("point_sale_number")));
                cardYards.add(cardYard);
            }
            return cardYards;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

    public static List<Executive> csvListExecutives(InputStream is) {
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setSkipHeaderRecord(true)
                .setIgnoreHeaderCase(true)
                .setTrim(true)
                .setDelimiter(';')
                .setHeader("id", "identification", "first_name", "last_name", "address", "phone_number", "conventional_telephone", "age", "yard_where_works")
                .build();
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)); CSVParser csvParser = new CSVParser(fileReader, csvFormat);) {
            List<Executive> customers = new ArrayList<>();
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                String yardWhereWorks = csvRecord.get("yard_where_works");
                CardYard cardYard = new CardYard();
                cardYard.setName(yardWhereWorks);
                Executive brand = new Executive()
                        .id(Long.parseLong(csvRecord.get("id")))
                        .identification(csvRecord.get("identification"))
                        .firstName(csvRecord.get("first_name"))
                        .lastName(csvRecord.get("last_name"))
                        .age(Integer.parseInt(csvRecord.get("age")))
                        .address(csvRecord.get("address"))
                        .conventionalTelephone(csvRecord.get("conventional_telephone"))
                        .yardWhereWorks(cardYard)
                        .phoneNumber(csvRecord.get("phone_number"));
                customers.add(brand);
            }
            return customers;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}

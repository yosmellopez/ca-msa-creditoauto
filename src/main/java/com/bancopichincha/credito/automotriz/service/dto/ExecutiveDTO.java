package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.pichincha.domain.Executive} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExecutiveDTO implements Serializable {

    private Long id;

    @NotNull
    private String identification;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String address;

    private String phoneNumber;

    private String conventionalTelephone;

    private Integer age;

    private CardYardDTO yardWhereWorks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getConventionalTelephone() {
        return conventionalTelephone;
    }

    public void setConventionalTelephone(String conventionalTelephone) {
        this.conventionalTelephone = conventionalTelephone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public CardYardDTO getYardWhereWorks() {
        return yardWhereWorks;
    }

    public void setYardWhereWorks(CardYardDTO yardWhereWorks) {
        this.yardWhereWorks = yardWhereWorks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExecutiveDTO)) {
            return false;
        }

        ExecutiveDTO executiveDTO = (ExecutiveDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, executiveDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExecutiveDTO{" +
                "id=" + getId() +
                ", identification='" + getIdentification() + "'" +
                ", firstName='" + getFirstName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", address='" + getAddress() + "'" +
                ", phoneNumber='" + getPhoneNumber() + "'" +
                ", conventionalTelephone='" + getConventionalTelephone() + "'" +
                ", age=" + getAge() +
                ", yardWhereWorks='" + getYardWhereWorks() + "'" +
                "}";
    }
}

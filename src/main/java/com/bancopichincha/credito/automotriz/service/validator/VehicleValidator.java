package com.bancopichincha.credito.automotriz.service.validator;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDTO;
import java.util.Optional;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class VehicleValidator implements Validator {

    private final VehicleRepository vehicleRepository;

    public VehicleValidator(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return VehicleDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Optional<VehicleDTO> dtoOptional = Optional.of(target)
                .filter(VehicleDTO.class::isInstance)
                .map(VehicleDTO.class::cast);
        dtoOptional.ifPresent(vehicle -> {
            Optional<Vehicle> vehicleOptional = vehicleRepository.findByLicensePlate(vehicle.getLicensePlate());
            vehicleOptional.ifPresent(request -> {
                errors.rejectValue("licensePlate", "No se permiten vehiculos con la misma placa");
            });
        });
    }
}

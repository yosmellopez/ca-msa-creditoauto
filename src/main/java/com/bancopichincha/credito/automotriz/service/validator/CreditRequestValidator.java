package com.bancopichincha.credito.automotriz.service.validator;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.domain.enumeration.CreditRequestStatus;
import com.bancopichincha.credito.automotriz.repository.*;
import com.bancopichincha.credito.automotriz.service.dto.*;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class CreditRequestValidator implements Validator {

    private final CreditRequestRepository requestRepository;

    private final VehicleRepository vehicleRepository;

    private final ExecutiveRepository executiveRepository;

    private final CardYardRepository cardYardRepository;

    private final CustomerRepository customerRepository;

    public CreditRequestValidator(CreditRequestRepository requestRepository, VehicleRepository vehicleRepository, ExecutiveRepository executiveRepository,
                                  CardYardRepository cardYardRepository, CustomerRepository customerRepository) {
        this.requestRepository = requestRepository;
        this.vehicleRepository = vehicleRepository;
        this.executiveRepository = executiveRepository;
        this.cardYardRepository = cardYardRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CreditRequestDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Optional<CreditRequestDTO> dtoOptional = Optional.of(target)
                .filter(CreditRequestDTO.class::isInstance)
                .map(CreditRequestDTO.class::cast);
        dtoOptional.ifPresent(credit -> {
            Long customerId = credit.getCustomer().getId();
            Optional<CreditRequest> requestCustomerOptional = requestRepository.findByCustomerIdAndElaborationDate(customerId, credit.getElaborationDate());
            requestCustomerOptional.ifPresent(request -> {
                if (request.getActive()) {
                    errors.rejectValue("active", "Ya existe una solicitud de credito para este cliente en esta fecha");
                }
            });
            Optional<Vehicle> vehicleOptional = Optional.ofNullable(credit.getVehicle())
                    .map(VehicleDTO::getId)
                    .flatMap(vehicleRepository::findById);
            if (vehicleOptional.isEmpty()) {
                errors.rejectValue("vehicle", "El vehiculo no existe");
            } else {
                Vehicle vehicle = vehicleOptional.get();
                List<CreditRequest> vehiclesForStatus = requestRepository.findByVehicleAndStatus(vehicle, CreditRequestStatus.REGISTERED);
                if (!vehiclesForStatus.isEmpty()) {
                    errors.rejectValue("vehicle", "El vehiculo no se puede vender porque ya se encuenta en reserva");
                }
            }
            Optional<CardYard> cardYardOptional = Optional.ofNullable(credit.getCardYard())
                    .map(CardYardDTO::getId)
                    .flatMap(cardYardRepository::findById);
            if (cardYardOptional.isEmpty()) {
                errors.rejectValue("cardYard", "El patio de autos no existe");
            }
            Optional<Executive> executiveOptional = Optional.ofNullable(credit.getExecutive())
                    .map(ExecutiveDTO::getId)
                    .flatMap(executiveRepository::findById);
            if (executiveOptional.isEmpty()) {
                errors.rejectValue("executive", "El ejecutivo de ventas no existe");
            }
            if (cardYardOptional.isPresent() && executiveOptional.isPresent()) {
                CardYard cardYard = cardYardOptional.get();
                Executive executive = executiveOptional.get();
                CardYard yardWhereWorks = executive.getYardWhereWorks();
                if (!Objects.equals(cardYard.getId(), yardWhereWorks.getId())) {
                    errors.rejectValue("executive", "El ejecutivo de ventas debe pertenecer al patio seleccionado");
                }
            }
            Optional<Customer> customerOptional = Optional.ofNullable(credit.getCustomer())
                    .map(CustomerDTO::getId)
                    .flatMap(customerRepository::findById);
            if (customerOptional.isEmpty()) {
                errors.rejectValue("customer", "El cliente no existe");
            }
        });
    }
}

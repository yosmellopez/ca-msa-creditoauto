package com.bancopichincha.credito.automotriz.service.validator;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class CustomerAssigmentValidator implements Validator {

    private final CardYardRepository cardYardRepository;

    private final CustomerRepository customerRepository;
    private final CustomerAssignmentRepository assignmentRepository;

    public CustomerAssigmentValidator(CardYardRepository cardYardRepository, CustomerRepository customerRepository, CustomerAssignmentRepository assignmentRepository) {
        this.cardYardRepository = cardYardRepository;
        this.customerRepository = customerRepository;
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CustomerAssignmentDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Optional<CustomerAssignmentDTO> dtoOptional = Optional.of(target)
                .filter(CustomerAssignmentDTO.class::isInstance)
                .map(CustomerAssignmentDTO.class::cast);
        dtoOptional.ifPresent(assignment -> {
            Optional<Customer> customerOptional = Optional.ofNullable(assignment.getCustomer())
                    .map(CustomerDTO::getId)
                    .flatMap(customerRepository::findById);
            if (customerOptional.isEmpty()) {
                errors.rejectValue("customer", "El cliente no existe");
            } else {
                Optional<CustomerAssignment> optional = assignmentRepository.findByCustomer(customerOptional.get());
                optional.ifPresent(c -> errors.rejectValue("customer", "El cliente ya existe en una solicitud"));
            }
            Optional<CardYard> cardYardOptional = Optional.ofNullable(assignment.getCardYard())
                    .map(CardYardDTO::getId)
                    .flatMap(cardYardRepository::findById);
            if (cardYardOptional.isEmpty()) {
                errors.rejectValue("cardYard", "El patio de autos no existe");
            }
        });
    }
}

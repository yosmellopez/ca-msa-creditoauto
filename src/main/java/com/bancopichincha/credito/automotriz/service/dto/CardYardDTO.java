package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.CardYard} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CardYardDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String address;

    @NotNull
    private Integer pointSaleNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPointSaleNumber() {
        return pointSaleNumber;
    }

    public void setPointSaleNumber(Integer pointSaleNumber) {
        this.pointSaleNumber = pointSaleNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CardYardDTO)) {
            return false;
        }

        CardYardDTO cardYardDTO = (CardYardDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cardYardDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CardYardDTO{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                ", phoneNumber='" + getPhoneNumber() + "'" +
                ", address='" + getAddress() + "'" +
                ", pointSaleNumber=" + getPointSaleNumber() +
                "}";
    }
}

package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.bancopichincha.credito.automotriz.domain.CustomerAssignment}.
 */
public interface CustomerAssignmentService {

    /**
     * Save a customerAssignment.
     *
     * @param customerAssignmentDTO the entity to save.
     * @return the persisted entity.
     */
    CustomerAssignmentDTO save(CustomerAssignmentDTO customerAssignmentDTO);

    /**
     * Updates a customerAssignment.
     *
     * @param customerAssignmentDTO the entity to update.
     * @return the persisted entity.
     */
    CustomerAssignmentDTO update(CustomerAssignmentDTO customerAssignmentDTO);

    /**
     * Partially updates a customerAssignment.
     *
     * @param customerAssignmentDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CustomerAssignmentDTO> partialUpdate(CustomerAssignmentDTO customerAssignmentDTO);

    /**
     * Get all the customerAssignments.
     *
     * @return the list of entities.
     */
    List<CustomerAssignmentDTO> findAll();

    /**
     * Get the "id" customerAssignment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CustomerAssignmentDTO> findOne(Long id);

    /**
     * Delete the "id" customerAssignment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.service.CSVHelper;
import com.bancopichincha.credito.automotriz.service.CardYardService;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CardYardMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CardYard}.
 */
@Service
@Transactional
public class CardYardServiceImpl implements CardYardService {

    private final Logger log = LoggerFactory.getLogger(CardYardServiceImpl.class);

    private final CardYardRepository cardYardRepository;

    private final CardYardMapper cardYardMapper;

    public CardYardServiceImpl(CardYardRepository cardYardRepository, CardYardMapper cardYardMapper) {
        this.cardYardRepository = cardYardRepository;
        this.cardYardMapper = cardYardMapper;
    }

    @Override
    public CardYardDTO save(CardYardDTO cardYardDTO) {
        log.debug("Request to save CardYard : {}", cardYardDTO);
        CardYard cardYard = cardYardMapper.toEntity(cardYardDTO);
        cardYard = cardYardRepository.save(cardYard);
        return cardYardMapper.toDto(cardYard);
    }

    @Override
    public CardYardDTO update(CardYardDTO cardYardDTO) {
        log.debug("Request to update CardYard : {}", cardYardDTO);
        CardYard cardYard = cardYardMapper.toEntity(cardYardDTO);
        cardYard = cardYardRepository.save(cardYard);
        return cardYardMapper.toDto(cardYard);
    }

    @Override
    public Optional<CardYardDTO> partialUpdate(CardYardDTO cardYardDTO) {
        log.debug("Request to partially update CardYard : {}", cardYardDTO);

        return cardYardRepository
                .findById(cardYardDTO.getId())
                .map(existingCardYard -> {
                    cardYardMapper.partialUpdate(existingCardYard, cardYardDTO);

                    return existingCardYard;
                })
                .map(cardYardRepository::save)
                .map(cardYardMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CardYardDTO> findAll() {
        log.debug("Request to get all CardYards");
        return cardYardRepository.findAll().stream().map(cardYardMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CardYardDTO> findOne(Long id) {
        log.debug("Request to get CardYard : {}", id);
        return cardYardRepository.findById(id).map(cardYardMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CardYard : {}", id);
        cardYardRepository.deleteById(id);
    }

    @Override
    public void initialLoad(File file) {
        try (InputStream inputStream = new FileInputStream(file)) {
            List<CardYard> cardYardList = CSVHelper.csvListCardYarns(inputStream);
            Set<CardYard> cardYards = cardYardList.stream()
                    .filter(cardYard -> cardYardRepository.findByName(cardYard.getName()).isEmpty())
                    .collect(Collectors.toSet());
            cardYardRepository.saveAll(cardYards);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

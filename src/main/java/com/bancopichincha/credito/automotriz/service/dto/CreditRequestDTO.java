package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enumeration.CreditRequestStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.CreditRequest} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CreditRequestDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate elaborationDate;

    @NotNull
    private Integer monthsTerm;

    @NotNull
    private Integer dues;

    @NotNull
    private Boolean active;

    @NotNull
    private BigDecimal initial;

    private String observation;

    @NotNull
    private VehicleDTO vehicle;

    @NotNull
    private ExecutiveDTO executive;

    @NotNull
    private CardYardDTO cardYard;

    @NotNull
    private CustomerDTO customer;

    private CreditRequestStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getElaborationDate() {
        return elaborationDate;
    }

    public void setElaborationDate(LocalDate elaborationDate) {
        this.elaborationDate = elaborationDate;
    }

    public Integer getMonthsTerm() {
        return monthsTerm;
    }

    public void setMonthsTerm(Integer monthsTerm) {
        this.monthsTerm = monthsTerm;
    }

    public Integer getDues() {
        return dues;
    }

    public void setDues(Integer dues) {
        this.dues = dues;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public BigDecimal getInitial() {
        return initial;
    }

    public void setInitial(BigDecimal initial) {
        this.initial = initial;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public VehicleDTO getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleDTO vehicle) {
        this.vehicle = vehicle;
    }

    public ExecutiveDTO getExecutive() {
        return executive;
    }

    public void setExecutive(ExecutiveDTO executive) {
        this.executive = executive;
    }

    public CardYardDTO getCardYard() {
        return cardYard;
    }

    public void setCardYard(CardYardDTO cardYard) {
        this.cardYard = cardYard;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public CreditRequestStatus getStatus() {
        return status;
    }

    public void setStatus(CreditRequestStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CreditRequestDTO)) {
            return false;
        }

        CreditRequestDTO creditRequestDTO = (CreditRequestDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, creditRequestDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CreditRequestDTO{" + "id=" + getId() + ", elaborationDate='" + getElaborationDate() + "'" + ", monthsTerm=" + getMonthsTerm() + ", dues=" + getDues() + ", initial=" + getInitial() + ", observation='" + getObservation() + "'" + ", vehicle=" + getVehicle() + ", executive=" + getExecutive() + ", cardYard=" + getCardYard() + ", customer=" + getCustomer() + "}";
    }
}

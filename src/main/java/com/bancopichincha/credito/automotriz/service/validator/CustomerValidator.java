package com.bancopichincha.credito.automotriz.service.validator;

import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerMapper;
import java.util.Objects;
import java.util.Optional;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CustomerValidator implements Validator {

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    public CustomerValidator(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CustomerDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Optional<CustomerDTO> dtoOptional = Optional.of(target)
                .filter(CustomerDTO.class::isInstance)
                .map(CustomerDTO.class::cast);
        dtoOptional.ifPresent(customer -> {
            Example<Customer> example = Example.of(customerMapper.toEntity(customer));
            Optional<Customer> customerOptional = customerRepository.findOne(example);
            customerOptional.ifPresent(request -> {
                Long customerId = customer.getId();
                Long id = request.getId();
                if (!Objects.equals(customerId, id)) errors.rejectValue("identification", "No se permiten clientes con el mismo identificador");
            });
        });
    }
}

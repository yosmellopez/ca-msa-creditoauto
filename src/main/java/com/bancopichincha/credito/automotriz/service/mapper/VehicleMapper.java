package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.CardYard;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Vehicle} and its DTO {@link VehicleDTO}.
 */
@Mapper(componentModel = "spring")
public interface VehicleMapper extends EntityMapper<VehicleDTO, Vehicle> {

//    @Mapping(target = "brand", source = "brand", qualifiedByName = "brandId")
//    @Mapping(target = "cardYards", source = "cardYards", qualifiedByName = "cardYardIdSet")
//    VehicleDTO toDto(Vehicle s);
//
//    @Mapping(target = "removeCardYard", ignore = true)
//    Vehicle toEntity(VehicleDTO vehicleDTO);
////
////    @Named("brandId")
////    @BeanMapping(ignoreByDefault = true)
////    @Mapping(target = "id", source = "id")
////    BrandDTO toDtoBrandId(Brand brand);
//
//    @Named("cardYardId")
//    @BeanMapping(ignoreByDefault = true)
//    @Mapping(target = "id", source = "id")
//    CardYardDTO toDtoCardYardId(CardYard cardYard);
//
//    @Named("cardYardIdSet")
//    default Set<CardYardDTO> toDtoCardYardIdSet(Set<CardYard> cardYard) {
//        return cardYard.stream().map(this::toDtoCardYardId).collect(Collectors.toSet());
//    }
}

package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.Vehicle} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class VehicleDTO implements Serializable {

    private Long id;

    @NotNull
    private String licensePlate;

    @NotNull
    private String model;

    @NotNull
    private Integer chasisNumber;

    private String type;

    @NotNull
    private Integer cylinderCapacity;

    @NotNull
    private String appraisal;

    private BrandDTO brand;

    private Set<CardYardDTO> cardYards = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getChasisNumber() {
        return chasisNumber;
    }

    public void setChasisNumber(Integer chasisNumber) {
        this.chasisNumber = chasisNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCylinderCapacity() {
        return cylinderCapacity;
    }

    public void setCylinderCapacity(Integer cylinderCapacity) {
        this.cylinderCapacity = cylinderCapacity;
    }

    public String getAppraisal() {
        return appraisal;
    }

    public void setAppraisal(String appraisal) {
        this.appraisal = appraisal;
    }

    public BrandDTO getBrand() {
        return brand;
    }

    public void setBrand(BrandDTO brand) {
        this.brand = brand;
    }

    public Set<CardYardDTO> getCardYards() {
        return cardYards;
    }

    public void setCardYards(Set<CardYardDTO> cardYards) {
        this.cardYards = cardYards;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VehicleDTO)) {
            return false;
        }

        VehicleDTO vehicleDTO = (VehicleDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, vehicleDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VehicleDTO{" +
                "id=" + getId() +
                ", licensePlate='" + getLicensePlate() + "'" +
                ", model='" + getModel() + "'" +
                ", chasisNumber=" + getChasisNumber() +
                ", type='" + getType() + "'" +
                ", cylinderCapacity=" + getCylinderCapacity() +
                ", appraisal='" + getAppraisal() + "'" +
                ", brand=" + getBrand() +
                ", cardYards=" + getCardYards() +
                "}";
    }
}

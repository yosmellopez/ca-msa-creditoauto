package com.bancopichincha.credito.automotriz.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.bancopichincha.credito.automotriz.domain.CustomerAssignment} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CustomerAssignmentDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate assignmentDate;

    @NotNull
    private CustomerDTO customer;

    @NotNull
    private CardYardDTO cardYard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(LocalDate assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public CardYardDTO getCardYard() {
        return cardYard;
    }

    public void setCardYard(CardYardDTO cardYard) {
        this.cardYard = cardYard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerAssignmentDTO)) {
            return false;
        }

        CustomerAssignmentDTO customerAssignmentDTO = (CustomerAssignmentDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, customerAssignmentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerAssignmentDTO{" +
                "id=" + getId() +
                ", assignmentDate='" + getAssignmentDate() + "'" +
                ", customer=" + getCustomer() +
                ", cardYard=" + getCardYard() +
                "}";
    }
}

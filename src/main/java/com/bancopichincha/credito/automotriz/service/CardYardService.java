package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.bancopichincha.credito.automotriz.domain.CardYard}.
 */
public interface CardYardService {

    /**
     * Save a cardYard.
     *
     * @param cardYardDTO the entity to save.
     * @return the persisted entity.
     */
    CardYardDTO save(CardYardDTO cardYardDTO);

    /**
     * Updates a cardYard.
     *
     * @param cardYardDTO the entity to update.
     * @return the persisted entity.
     */
    CardYardDTO update(CardYardDTO cardYardDTO);

    /**
     * Partially updates a cardYard.
     *
     * @param cardYardDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CardYardDTO> partialUpdate(CardYardDTO cardYardDTO);

    /**
     * Get all the cardYards.
     *
     * @return the list of entities.
     */
    List<CardYardDTO> findAll();

    /**
     * Get the "id" cardYard.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CardYardDTO> findOne(Long id);

    /**
     * Delete the "id" cardYard.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void initialLoad(File file);
}

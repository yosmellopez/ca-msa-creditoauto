package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CreditRequest;
import com.bancopichincha.credito.automotriz.domain.enumeration.CreditRequestStatus;
import com.bancopichincha.credito.automotriz.repository.CreditRequestRepository;
import com.bancopichincha.credito.automotriz.service.CreditRequestService;
import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CreditRequestDTO;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.mapper.CreditRequestMapper;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CreditRequest}.
 */
@Service
@Transactional
public class CreditRequestServiceImpl implements CreditRequestService {

    private final Logger log = LoggerFactory.getLogger(CreditRequestServiceImpl.class);

    private final CreditRequestRepository creditRequestRepository;

    private final CreditRequestMapper creditRequestMapper;

    private final CustomerAssignmentService assignmentService;

    public CreditRequestServiceImpl(CreditRequestRepository creditRequestRepository, CreditRequestMapper creditRequestMapper, CustomerAssignmentService assignmentService) {
        this.creditRequestRepository = creditRequestRepository;
        this.creditRequestMapper = creditRequestMapper;
        this.assignmentService = assignmentService;
    }

    @Override
    public CreditRequestDTO save(CreditRequestDTO creditRequestDTO) {
        log.debug("Request to save CreditRequest : {}", creditRequestDTO);
        CreditRequest creditRequest = creditRequestMapper.toEntity(creditRequestDTO);
        creditRequest.setStatus(CreditRequestStatus.REGISTERED);
        creditRequest = creditRequestRepository.save(creditRequest);
        CreditRequestDTO requestDTO = creditRequestMapper.toDto(creditRequest);
        CustomerAssignmentDTO assignment = new CustomerAssignmentDTO();
        assignment.setCardYard(requestDTO.getCardYard());
        assignment.setCustomer(requestDTO.getCustomer());
        assignment.setAssignmentDate(LocalDate.now());
        assignmentService.save(assignment);
        return requestDTO;
    }

    @Override
    public CreditRequestDTO update(CreditRequestDTO creditRequestDTO) {
        log.debug("Request to update CreditRequest : {}", creditRequestDTO);
        CreditRequest creditRequest = creditRequestMapper.toEntity(creditRequestDTO);
        creditRequest = creditRequestRepository.save(creditRequest);
        return creditRequestMapper.toDto(creditRequest);
    }

    @Override
    public Optional<CreditRequestDTO> partialUpdate(CreditRequestDTO creditRequestDTO) {
        log.debug("Request to partially update CreditRequest : {}", creditRequestDTO);

        return creditRequestRepository.findById(creditRequestDTO.getId()).map(existingCreditRequest -> {
            creditRequestMapper.partialUpdate(existingCreditRequest, creditRequestDTO);

            return existingCreditRequest;
        }).map(creditRequestRepository::save).map(creditRequestMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CreditRequestDTO> findAll() {
        log.debug("Request to get all CreditRequests");
        return creditRequestRepository.findAll().stream().map(creditRequestMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CreditRequestDTO> findOne(Long id) {
        log.debug("Request to get CreditRequest : {}", id);
        return creditRequestRepository.findById(id).map(creditRequestMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CreditRequest : {}", id);
        creditRequestRepository.deleteById(id);
    }

    @Override
    public void cancel(Long id) {
        creditRequestRepository.findById(id).map(existingCreditRequest -> {
            existingCreditRequest.setStatus(CreditRequestStatus.CANCELED);
            return existingCreditRequest;
        }).map(creditRequestRepository::save);
    }

    @Override
    public void dispatch(Long id) {
        creditRequestRepository.findById(id).map(existingCreditRequest -> {
            existingCreditRequest.setStatus(CreditRequestStatus.DISPATCHED);
            return existingCreditRequest;
        }).map(creditRequestRepository::save);
    }
}

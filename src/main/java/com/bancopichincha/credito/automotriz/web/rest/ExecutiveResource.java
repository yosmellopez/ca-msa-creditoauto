package com.bancopichincha.credito.automotriz.web.rest;

import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bancopichincha.credito.automotriz.domain.Executive}.
 */
@RestController
@RequestMapping("/api")
public class ExecutiveResource {

    private final Logger log = LoggerFactory.getLogger(ExecutiveResource.class);

    private static final String ENTITY_NAME = "executive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExecutiveService executiveService;

    private final ExecutiveRepository executiveRepository;

    public ExecutiveResource(ExecutiveService executiveService, ExecutiveRepository executiveRepository) {
        this.executiveService = executiveService;
        this.executiveRepository = executiveRepository;
    }

    /**
     * {@code POST  /executives} : Create a new executive.
     *
     * @param executiveDTO the executiveDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new executiveDTO, or with status {@code 400 (Bad Request)} if the executive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/executives")
    public ResponseEntity<ExecutiveDTO> createExecutive(@Valid @RequestBody ExecutiveDTO executiveDTO) throws URISyntaxException {
        log.debug("REST request to save Executive : {}", executiveDTO);
        if (executiveDTO.getId() != null) {
            throw new BadRequestAlertException("A new executive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExecutiveDTO result = executiveService.save(executiveDTO);
        return ResponseEntity
                .created(new URI("/api/executives/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /executives/:id} : Updates an existing executive.
     *
     * @param id the id of the executiveDTO to save.
     * @param executiveDTO the executiveDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated executiveDTO,
     * or with status {@code 400 (Bad Request)} if the executiveDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the executiveDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/executives/{id}")
    public ResponseEntity<ExecutiveDTO> updateExecutive(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody ExecutiveDTO executiveDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Executive : {}, {}", id, executiveDTO);
        if (executiveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, executiveDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!executiveRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExecutiveDTO result = executiveService.update(executiveDTO);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, executiveDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code PATCH  /executives/:id} : Partial updates given fields of an existing executive, field will ignore if it is null
     *
     * @param id the id of the executiveDTO to save.
     * @param executiveDTO the executiveDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated executiveDTO,
     * or with status {@code 400 (Bad Request)} if the executiveDTO is not valid,
     * or with status {@code 404 (Not Found)} if the executiveDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the executiveDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/executives/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<ExecutiveDTO> partialUpdateExecutive(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody ExecutiveDTO executiveDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Executive partially : {}, {}", id, executiveDTO);
        if (executiveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, executiveDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!executiveRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExecutiveDTO> result = executiveService.partialUpdate(executiveDTO);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, executiveDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /executives} : get all the executives.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of executives in body.
     */
    @GetMapping("/executives")
    public List<ExecutiveDTO> getAllExecutives() {
        log.debug("REST request to get all Executives");
        return executiveService.findAll();
    }

    /**
     * {@code GET  /executives/:id} : get the "id" executive.
     *
     * @param id the id of the executiveDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the executiveDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/executives/{id}")
    public ResponseEntity<ExecutiveDTO> getExecutive(@PathVariable Long id) {
        log.debug("REST request to get Executive : {}", id);
        Optional<ExecutiveDTO> executiveDTO = executiveService.findOne(id);
        return ResponseUtil.wrapOrNotFound(executiveDTO);
    }

    /**
     * {@code DELETE  /executives/:id} : delete the "id" executive.
     *
     * @param id the id of the executiveDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/executives/{id}")
    public ResponseEntity<Void> deleteExecutive(@PathVariable Long id) {
        log.debug("REST request to delete Executive : {}", id);
        executiveService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                .build();
    }
}

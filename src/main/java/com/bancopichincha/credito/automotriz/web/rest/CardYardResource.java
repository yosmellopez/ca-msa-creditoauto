package com.bancopichincha.credito.automotriz.web.rest;

import com.bancopichincha.credito.automotriz.repository.CardYardRepository;
import com.bancopichincha.credito.automotriz.service.CardYardService;
import com.bancopichincha.credito.automotriz.service.dto.CardYardDTO;
import com.bancopichincha.credito.automotriz.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bancopichincha.credito.automotriz.domain.CardYard}.
 */
@RestController
@RequestMapping("/api")
public class CardYardResource {

    private final Logger log = LoggerFactory.getLogger(CardYardResource.class);

    private static final String ENTITY_NAME = "cardYard";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CardYardService cardYardService;

    private final CardYardRepository cardYardRepository;

    public CardYardResource(CardYardService cardYardService, CardYardRepository cardYardRepository) {
        this.cardYardService = cardYardService;
        this.cardYardRepository = cardYardRepository;
    }

    /**
     * {@code POST  /card-yards} : Create a new cardYard.
     *
     * @param cardYardDTO the cardYardDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cardYardDTO, or with status {@code 400 (Bad Request)} if the cardYard has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/card-yards")
    public ResponseEntity<CardYardDTO> createCardYard(@Valid @RequestBody CardYardDTO cardYardDTO) throws URISyntaxException {
        log.debug("REST request to save CardYard : {}", cardYardDTO);
        if (cardYardDTO.getId() != null) {
            throw new BadRequestAlertException("A new cardYard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CardYardDTO result = cardYardService.save(cardYardDTO);
        return ResponseEntity
                .created(new URI("/api/card-yards/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /card-yards/:id} : Updates an existing cardYard.
     *
     * @param id the id of the cardYardDTO to save.
     * @param cardYardDTO the cardYardDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cardYardDTO,
     * or with status {@code 400 (Bad Request)} if the cardYardDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cardYardDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/card-yards/{id}")
    public ResponseEntity<CardYardDTO> updateCardYard(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody CardYardDTO cardYardDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CardYard : {}, {}", id, cardYardDTO);
        if (cardYardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cardYardDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cardYardRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CardYardDTO result = cardYardService.update(cardYardDTO);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cardYardDTO.getId().toString()))
                .body(result);
    }

    /**
     * {@code PATCH  /card-yards/:id} : Partial updates given fields of an existing cardYard, field will ignore if it is null
     *
     * @param id the id of the cardYardDTO to save.
     * @param cardYardDTO the cardYardDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cardYardDTO,
     * or with status {@code 400 (Bad Request)} if the cardYardDTO is not valid,
     * or with status {@code 404 (Not Found)} if the cardYardDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the cardYardDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/card-yards/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<CardYardDTO> partialUpdateCardYard(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody CardYardDTO cardYardDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CardYard partially : {}, {}", id, cardYardDTO);
        if (cardYardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cardYardDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cardYardRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CardYardDTO> result = cardYardService.partialUpdate(cardYardDTO);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cardYardDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /card-yards} : get all the cardYards.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cardYards in body.
     */
    @GetMapping("/card-yards")
    public List<CardYardDTO> getAllCardYards() {
        log.debug("REST request to get all CardYards");
        return cardYardService.findAll();
    }

    /**
     * {@code GET  /card-yards/:id} : get the "id" cardYard.
     *
     * @param id the id of the cardYardDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cardYardDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/card-yards/{id}")
    public ResponseEntity<CardYardDTO> getCardYard(@PathVariable Long id) {
        log.debug("REST request to get CardYard : {}", id);
        Optional<CardYardDTO> cardYardDTO = cardYardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cardYardDTO);
    }

    /**
     * {@code DELETE  /card-yards/:id} : delete the "id" cardYard.
     *
     * @param id the id of the cardYardDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/card-yards/{id}")
    public ResponseEntity<Void> deleteCardYard(@PathVariable Long id) {
        log.debug("REST request to delete CardYard : {}", id);
        cardYardService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                .build();
    }
}

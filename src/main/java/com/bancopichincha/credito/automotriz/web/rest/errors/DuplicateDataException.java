package com.bancopichincha.credito.automotriz.web.rest.errors;

public class DuplicateDataException extends RuntimeException{

    public DuplicateDataException(String message) {
        super(message);
    }
}

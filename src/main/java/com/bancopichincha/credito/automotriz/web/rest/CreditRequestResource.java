package com.bancopichincha.credito.automotriz.web.rest;

import com.bancopichincha.credito.automotriz.repository.CreditRequestRepository;
import com.bancopichincha.credito.automotriz.service.CreditRequestService;
import com.bancopichincha.credito.automotriz.service.dto.CreditRequestDTO;
import com.bancopichincha.credito.automotriz.service.validator.CreditRequestValidator;
import com.bancopichincha.credito.automotriz.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bancopichincha.credito.automotriz.domain.CreditRequest}.
 */
@RestController
@RequestMapping("/api")
public class CreditRequestResource {

    private final Logger log = LoggerFactory.getLogger(CreditRequestResource.class);

    private static final String ENTITY_NAME = "creditRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CreditRequestService creditRequestService;

    private final CreditRequestRepository creditRequestRepository;

    private final CreditRequestValidator creditRequestValidator;

    public CreditRequestResource(CreditRequestService creditRequestService, CreditRequestRepository creditRequestRepository, CreditRequestValidator creditRequestValidator) {
        this.creditRequestService = creditRequestService;
        this.creditRequestRepository = creditRequestRepository;
        this.creditRequestValidator = creditRequestValidator;
    }

    /**
     * {@code POST  /credit-requests} : Create a new creditRequest.
     *
     * @param creditRequestDTO the creditRequestDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new creditRequestDTO, or with status {@code 400 (Bad Request)} if the creditRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/credit-requests")
    public ResponseEntity<CreditRequestDTO> createCreditRequest(@Valid @RequestBody CreditRequestDTO creditRequestDTO)
            throws URISyntaxException {
        log.debug("REST request to save CreditRequest : {}", creditRequestDTO);
        if (creditRequestDTO.getId() != null) {
            throw new BadRequestAlertException("A new creditRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CreditRequestDTO result = creditRequestService.save(creditRequestDTO);
        return ResponseEntity
                .created(new URI("/api/credit-requests/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /credit-requests/:id} : Updates an existing creditRequest.
     *
     * @param id the id of the creditRequestDTO to save.
     * @param creditRequestDTO the creditRequestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creditRequestDTO,
     * or with status {@code 400 (Bad Request)} if the creditRequestDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the creditRequestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/credit-requests/{id}")
    public ResponseEntity<CreditRequestDTO> updateCreditRequest(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody CreditRequestDTO creditRequestDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CreditRequest : {}, {}", id, creditRequestDTO);
        if (creditRequestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creditRequestDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creditRequestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CreditRequestDTO result = creditRequestService.update(creditRequestDTO);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, creditRequestDTO.getId().toString()))
                .body(result);
    }

    @PutMapping("/credit-requests/cancel/{id}")
    public ResponseEntity<Void> cancelCreditRequest(@PathVariable(value = "id", required = false) final Long id) {
        log.debug("REST request to update CreditRequest : {}", id);
        if (!creditRequestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        creditRequestService.cancel(id);
        return ResponseEntity.noContent()
                .build();
    }

    @PutMapping("/credit-requests/dispatch/{id}")
    public ResponseEntity<Void> dispatchedCreditRequest(@PathVariable(value = "id", required = false) final Long id) {
        log.debug("REST request to update CreditRequest : {}", id);
        if (!creditRequestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        creditRequestService.dispatch(id);
        return ResponseEntity.noContent()
                .build();
    }

    /**
     * {@code PATCH  /credit-requests/:id} : Partial updates given fields of an existing creditRequest, field will ignore if it is null
     *
     * @param id the id of the creditRequestDTO to save.
     * @param creditRequestDTO the creditRequestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated creditRequestDTO,
     * or with status {@code 400 (Bad Request)} if the creditRequestDTO is not valid,
     * or with status {@code 404 (Not Found)} if the creditRequestDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the creditRequestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/credit-requests/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<CreditRequestDTO> partialUpdateCreditRequest(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody CreditRequestDTO creditRequestDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CreditRequest partially : {}, {}", id, creditRequestDTO);
        if (creditRequestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, creditRequestDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!creditRequestRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CreditRequestDTO> result = creditRequestService.partialUpdate(creditRequestDTO);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, creditRequestDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /credit-requests} : get all the creditRequests.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of creditRequests in body.
     */
    @GetMapping("/credit-requests")
    public List<CreditRequestDTO> getAllCreditRequests() {
        log.debug("REST request to get all CreditRequests");
        return creditRequestService.findAll();
    }

    /**
     * {@code GET  /credit-requests/:id} : get the "id" creditRequest.
     *
     * @param id the id of the creditRequestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the creditRequestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/credit-requests/{id}")
    public ResponseEntity<CreditRequestDTO> getCreditRequest(@PathVariable Long id) {
        log.debug("REST request to get CreditRequest : {}", id);
        Optional<CreditRequestDTO> creditRequestDTO = creditRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(creditRequestDTO);
    }

    /**
     * {@code DELETE  /credit-requests/:id} : delete the "id" creditRequest.
     *
     * @param id the id of the creditRequestDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/credit-requests/{id}")
    public ResponseEntity<Void> deleteCreditRequest(@PathVariable Long id) {
        log.debug("REST request to delete CreditRequest : {}", id);
        creditRequestService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
                .build();
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        Optional.ofNullable(binder.getTarget()).ifPresent(target -> {
            if (creditRequestValidator.supports(target.getClass())) {
                binder.addValidators(creditRequestValidator);
            }
        });
    }
}

package com.bancopichincha.credito.automotriz.web.rest;

import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDTO;
import com.bancopichincha.credito.automotriz.service.validator.CustomerAssigmentValidator;
import com.bancopichincha.credito.automotriz.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bancopichincha.credito.automotriz.domain.CustomerAssignment}.
 */
@RestController
@RequestMapping("/api")
public class CustomerAssignmentResource {

    private final Logger log = LoggerFactory.getLogger(CustomerAssignmentResource.class);

    private static final String ENTITY_NAME = "customerAssignment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerAssignmentService customerAssignmentService;

    private final CustomerAssignmentRepository customerAssignmentRepository;

    private final CustomerAssigmentValidator assigmentValidator;

    public CustomerAssignmentResource(CustomerAssignmentService customerAssignmentService, CustomerAssignmentRepository customerAssignmentRepository, CustomerAssigmentValidator assigmentValidator) {
        this.customerAssignmentService = customerAssignmentService;
        this.customerAssignmentRepository = customerAssignmentRepository;
        this.assigmentValidator = assigmentValidator;
    }

    /**
     * {@code POST  /customer-assignments} : Create a new customerAssignment.
     *
     * @param customerAssignmentDTO the customerAssignmentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerAssignmentDTO, or with status {@code 400 (Bad Request)} if the customerAssignment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer-assignments")
    public ResponseEntity<CustomerAssignmentDTO> createCustomerAssignment(@Valid @RequestBody CustomerAssignmentDTO customerAssignmentDTO) throws URISyntaxException {
        log.debug("REST request to save CustomerAssignment : {}", customerAssignmentDTO);
        if (customerAssignmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new customerAssignment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerAssignmentDTO result = customerAssignmentService.save(customerAssignmentDTO);
        return ResponseEntity.created(new URI("/api/customer-assignments/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * {@code PUT  /customer-assignments/:id} : Updates an existing customerAssignment.
     *
     * @param id the id of the customerAssignmentDTO to save.
     * @param customerAssignmentDTO the customerAssignmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerAssignmentDTO,
     * or with status {@code 400 (Bad Request)} if the customerAssignmentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerAssignmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customer-assignments/{id}")
    public ResponseEntity<CustomerAssignmentDTO> updateCustomerAssignment(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody CustomerAssignmentDTO customerAssignmentDTO) throws URISyntaxException {
        log.debug("REST request to update CustomerAssignment : {}, {}", id, customerAssignmentDTO);
        if (customerAssignmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerAssignmentDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerAssignmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CustomerAssignmentDTO result = customerAssignmentService.update(customerAssignmentDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerAssignmentDTO.getId().toString())).body(result);
    }

    /**
     * {@code PATCH  /customer-assignments/:id} : Partial updates given fields of an existing customerAssignment, field will ignore if it is null
     *
     * @param id the id of the customerAssignmentDTO to save.
     * @param customerAssignmentDTO the customerAssignmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerAssignmentDTO,
     * or with status {@code 400 (Bad Request)} if the customerAssignmentDTO is not valid,
     * or with status {@code 404 (Not Found)} if the customerAssignmentDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the customerAssignmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/customer-assignments/{id}", consumes = {"application/json", "application/merge-patch+json"})
    public ResponseEntity<CustomerAssignmentDTO> partialUpdateCustomerAssignment(@PathVariable(value = "id", required = false) final Long id, @NotNull @RequestBody CustomerAssignmentDTO customerAssignmentDTO) throws URISyntaxException {
        log.debug("REST request to partial update CustomerAssignment partially : {}, {}", id, customerAssignmentDTO);
        if (customerAssignmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customerAssignmentDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerAssignmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CustomerAssignmentDTO> result = customerAssignmentService.partialUpdate(customerAssignmentDTO);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerAssignmentDTO.getId().toString()));
    }

    /**
     * {@code GET  /customer-assignments} : get all the customerAssignments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerAssignments in body.
     */
    @GetMapping("/customer-assignments")
    public List<CustomerAssignmentDTO> getAllCustomerAssignments() {
        log.debug("REST request to get all CustomerAssignments");
        return customerAssignmentService.findAll();
    }

    /**
     * {@code GET  /customer-assignments/:id} : get the "id" customerAssignment.
     *
     * @param id the id of the customerAssignmentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerAssignmentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer-assignments/{id}")
    public ResponseEntity<CustomerAssignmentDTO> getCustomerAssignment(@PathVariable Long id) {
        log.debug("REST request to get CustomerAssignment : {}", id);
        Optional<CustomerAssignmentDTO> customerAssignmentDTO = customerAssignmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(customerAssignmentDTO);
    }

    /**
     * {@code DELETE  /customer-assignments/:id} : delete the "id" customerAssignment.
     *
     * @param id the id of the customerAssignmentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer-assignments/{id}")
    public ResponseEntity<Void> deleteCustomerAssignment(@PathVariable Long id) {
        log.debug("REST request to delete CustomerAssignment : {}", id);
        customerAssignmentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        Optional.ofNullable(binder.getTarget()).ifPresent(target -> {
            if (assigmentValidator.supports(target.getClass())) {
                binder.addValidators(assigmentValidator);
            }
        });
    }
}

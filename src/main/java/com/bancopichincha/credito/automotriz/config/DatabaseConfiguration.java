package com.bancopichincha.credito.automotriz.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories({"com.bancopichincha.credito.automotriz.repository"})
@EnableTransactionManagement
public class DatabaseConfiguration {

}

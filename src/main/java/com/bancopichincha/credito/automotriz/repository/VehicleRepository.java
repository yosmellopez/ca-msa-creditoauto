package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Vehicle entity.
 * <p>
 * When extending this class, extend VehicleRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface VehicleRepository extends VehicleRepositoryWithBagRelationships, JpaRepository<Vehicle, Long> {

    default Optional<Vehicle> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<Vehicle> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Vehicle> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    Optional<Vehicle> findByLicensePlate(String licensePlate);

    @EntityGraph(attributePaths = {"cardYards", "brand"})
    List<Vehicle> findByModel(String model);

    @EntityGraph(attributePaths = {"cardYards", "brand"})
    Optional<Vehicle> findByBrandId(Long id);
}

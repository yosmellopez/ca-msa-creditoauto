package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the CustomerAssignment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerAssignmentRepository extends JpaRepository<CustomerAssignment, Long> {
    Optional<CustomerAssignment> findByCustomer(Customer customer);
}

package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class VehicleRepositoryWithBagRelationshipsImpl implements VehicleRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Vehicle> fetchBagRelationships(Optional<Vehicle> vehicle) {
        return vehicle.map(this::fetchCardYards);
    }

    @Override
    public Page<Vehicle> fetchBagRelationships(Page<Vehicle> vehicles) {
        return new PageImpl<>(fetchBagRelationships(vehicles.getContent()), vehicles.getPageable(), vehicles.getTotalElements());
    }

    @Override
    public List<Vehicle> fetchBagRelationships(List<Vehicle> vehicles) {
        return Optional.of(vehicles).map(this::fetchCardYards).orElse(Collections.emptyList());
    }

    Vehicle fetchCardYards(Vehicle result) {
        return entityManager
                .createQuery("select vehicle from Vehicle vehicle left join fetch vehicle.cardYards where vehicle is :vehicle", Vehicle.class)
                .setParameter("vehicle", result)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getSingleResult();
    }

    List<Vehicle> fetchCardYards(List<Vehicle> vehicles) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, vehicles.size()).forEach(index -> order.put(vehicles.get(index).getId(), index));
        List<Vehicle> result = entityManager
                .createQuery(
                        "select distinct vehicle from Vehicle vehicle left join fetch vehicle.cardYards where vehicle in :vehicles",
                        Vehicle.class
                )
                .setParameter("vehicles", vehicles)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}

package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CreditRequest;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.domain.enumeration.CreditRequestStatus;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CreditRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CreditRequestRepository extends JpaRepository<CreditRequest, Long> {

    Optional<CreditRequest> findByCustomerIdAndElaborationDate(Long customerId, LocalDate elaborationDate);

    List<CreditRequest> findByVehicleAndStatus(Vehicle vehicle, CreditRequestStatus status);
}

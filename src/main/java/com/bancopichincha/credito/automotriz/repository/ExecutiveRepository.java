package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Executive;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Executive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExecutiveRepository extends JpaRepository<Executive, Long> {

    Optional<Executive> findByIdentification(String identification);

}

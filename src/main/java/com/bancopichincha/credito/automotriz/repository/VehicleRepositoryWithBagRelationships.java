package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface VehicleRepositoryWithBagRelationships {

    Optional<Vehicle> fetchBagRelationships(Optional<Vehicle> vehicle);

    List<Vehicle> fetchBagRelationships(List<Vehicle> vehicles);

    Page<Vehicle> fetchBagRelationships(Page<Vehicle> vehicles);
}

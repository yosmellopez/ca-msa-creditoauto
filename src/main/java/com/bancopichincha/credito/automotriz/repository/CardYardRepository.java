package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CardYard;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CardYard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CardYardRepository extends JpaRepository<CardYard, Long> {

    Optional<CardYard> findByName(String name);
}
